---
layout: home
title: Nicolas Sauret
---

> « Vous venez, vous aussi, prendre la mesure du monde ? »
>  
> -- Manu Larcenet, *Le Rapport Brodeck*
{: .home-citation}
