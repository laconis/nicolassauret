---
layout: page
title: En deux mots 
notoc: true
---

{: .droite}
<button class="lang-button">
[EN](./en/)
</button>

Je suis maître de conférences à l'Université Paris 8 au département _Humanités numériques_, où j'enseigne les sciences de l'information et de la communication.

Mes recherches portent sur les écritures, quand elles sont collectives, collaboratives, quand elles réinventent les savoirs, leurs productions et leurs circulations. Démarche récursive ou recherche-active de fabriques éditoriales et scripturales avec et pour les communautés qui m'inspirent `#communs` `#litterature` `#gitterature` `#etudiant·e·s` `#chercheur·e·s`. Le numérique comme objet de recherche et comme espace d'action `#libre` et `#lowtech`. 

[Pour en savoir plus](/recherches/).


### mon parcours

En tant que post-doctorant, j'ai partagé mon temps entre :

- le [FabPart Lab](https://fplab.parisnanterre.fr), projet transversal au [labex _Les passés dans le présent_](https://passes-present.eu). Sous la direction de [Marta Severo](https://www.martasevero.com/about/), le FP Lab a accompagné les chercheur·e·s et les étudiant·e·s pour favoriser les approches de médiation, de participation et de valorisation dans leurs projets&nbsp;;
- le [HN Lab](https://www.huma-num.fr/hnlab/) de la TGIR Huma-Num, sous la direction de [Stéphane Pouyllau](https://www.huma-num.fr/stephane-pouyllau/), qui explore les approches, les outils et les services émergents dans la communauté SHS.

J'ai obtenu en novembre 2020 un doctorat en Sciences de l'information et de la communication (Université Paris Nanterre) et en Littérature comparée (Université de Montréal), pour une thèse intitulée «&nbsp;De la revue au collectif&nbsp;: la conversation comme dispositif d’éditorialisation des communautés savantes en lettres et sciences humaines&nbsp;» ([these.nicolassauret.net](https://these.nicolassauret.net/)). Celle-ci a été co-dirigée par [Marcello Vitali-Rosati](http://vitalirosati.com), professeur au département des littératures de langue française de l’Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures numériques et par [Manuel Zacklad](https://www.dicen-idf.org/membre/zacklad-manuel/), professeur de Sciences de l’information et de la communication au CNAM et directeur du laboratoire Dicen-IDF. [Louise Merzeau &#x271d;](http://merzeau.net/), anciennement professeure de Sciences de l’information et de la communication à l’Université Paris Nanterre et co-directrice du laboratoire Dicen-IDF, a initié la co-direction en 2015 avant de nous quitter en juillet 2017.

Le doctorat a été réalisé dans le cadre du labex [Les passés dans le présent](https://passes-present.eu) et a donc bénéficié de l’aide de l’État géré par l’ANR au titre du programme _Investissements d’avenir_ portant la référence ANR-11-LABX-0026-01. Elle a également bénéficié du soutien de la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca), du [FRQSC](https://frqsc.gouv.qc.ca/) (Programme international) et du [CRIHN](https://crihn.org).


Jusqu'en 2016, j'étais ingénieur de recherche à l'[IRI](http://www.iri.centrepompidou.fr), mobilisé sur :

* la gestion de projets de recherche appliqués à l'éducation, la muséologie, les médias, le spectacle vivant, et les institutions culturelles en général
* la conception de prototypes, de services web, multimédia et transmédia, avec une approche spécialisée sur l'indexation, l'annotation et l'éditorialisation, pour la mise en oeuvre de dispositifs de contribution, d'écriture et de collection
* l'organisation du séminaire [*Écritures numériques et éditorialisation*](http://seminaire.sens-public.org) (2010-2016)
* ainsi que la coordination de l'atelier [Design Metadata](http://www.iri.centrepompidou.fr/ateliers/design-metadata-2013-2/) (2011-2014)

En 2009, j'ai créé avec deux associés la société de production Inflammable Productions, spécialisée dans les nouveaux médias et les nouvelles formes de narration.  
*Références 2009 à 2011 : Lemonde.fr, Arte, Nokia, La Ville de Paris, Forum des Images*

J'ai travaillé comme vidéaste et réalisateur de documentaire en Asie pendant 6 ans, de 2003 à 2008, au Laos puis à Hong Kong. Mon travail de réalisation a été diffusé dans plusieurs expositions et festivals.  
*Récompenses & sélections : Cinéma du Réel, Videoformes, Hong Kong Asian Film Festival, Tokyo Video Festival, Urban Nomad Festival, I shot Hong Kong Film Festival, Biennale de Hong Kong*  
*Références : Connaissances des Arts, UNESCO, Goethe-Institut, Omega, Adidas, EMI, UNICEF, Médecins du Monde*

### études
J'ai un _Master of fine arts_ (2007) de la School of Creative Media de la City University of Hong Kong, et je suis ingénieur, diplômé en 2002 de l'Université de Technologie de Compiègne.
