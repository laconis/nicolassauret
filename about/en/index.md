---
layout: page
title: In a nutshell 
notoc: true
---


{: .droite}
<button class="lang-button">
[FR](/about/)
</button>




I'm a Associate Professor at Université Paris 8 in [Paragraphe Lab](https://paragraphe.univ-paris8.fr/), and in the [Digital Humanities Department](https://humanites-numeriques.univ-paris8.fr/), where I teach information and communication sciences.

My research focuses on the collective nature of knowledge production circulation and legitimization. Between theoretical research and publishing experimentation, my work explores the practices of digital writing practices and new knowledge factories, whether open, collective or participatory.

My PhD explored more specifically practices and protocols of scholarly writing and publishing. I defended a thesis entitled [“From journal to collective: the conversation as editorialization dispositif of scholarly communities in the humanities and social sciences”](https://these.nicolassauret.net). 

Since then, I adopt a recursive, action-research approach to editing and writing, with and for the communities that inspire me: `#commons`, `#literature`, `#giterature`, `#students`, `#scholars`. 

From 2020 to 2022, I divided my time between the [HN Lab](https://hnlab.huma-num.fr/) at TGIR Huma-num and the [FabPart Lab](https://fplab.parisnanterre.fr/) at Labex _Les passés dans le présent_. As a post-doctoral researcher and research engineer, I collaborated with Marta Severo to coordinate the FabPart Lab, which aims to establish a factory for digital mediation and participation in the fields of memory and heritage. On the other hand, I participated in the HN Lab with Stéphane Pouyllau and Mélanie Bunel, involved in identifying and experimenting prospective tools for scholars in the humanities and social sciences.


Thinking the digital as an object of research and as a space for `#free` and `#lowtech` action. 

[More information (fr)](/recherches/).

