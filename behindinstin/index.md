---
layout: page
title: Behind Instin
---

_Behind Instin_ est un carnet de recherches dédié à l'objet littéraire **Général Instin**, et animé par Servanne Monjour et Nicolas Sauret.

**Servanne Monjour** est maîtresse de conférence à Sorbonne Université. [Ses travaux](https://corpuces.net) portent sur les corpus littéraires numériques.

### About Général Instin

« What the fuck is _Général Instin_ ? ». Tentative de définition sur [ce post]({% post_url behindinstin/2017-02-22-projet-instin-notes-preliminaires-pour-l-etude-d-une-litterature-brouhaha %}).




[nicolas]:http://nicolassauret.net
[senspublic]:http://sens-public.org/

### Carnet

<div class="posts">
  <ul class="post">
  {% for post in site.categories.behindinstin %}
      <li>
      <a href="{{ post.url }}">
        {{ post.title }}
      </a>
    <span class="post-date">{{ post.date | date_to_string }}</span>
    </li>
    <!-- {{ post.content }} -->
  {% endfor %}
  </ul>
</div>
