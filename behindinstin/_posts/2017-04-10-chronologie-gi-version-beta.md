---
layout: post
title: Chronologie GI (version bêta)
date: '2017-04-10 22:12'
category: behindinstin
---

Notre première Timeline nous a permis de visualiser un échantillon de l'archive publiée sur remue.net. Cette première visualisation s'est révélée très utile, car elle a mis en évidence un certain nombre de lacunes ou d'erreurs (des item non classés dans leur rubrique d'origine, par exemple) et nous a conduit à affiner et à enrichir notre inventaire.

Nous avons donc réalisé une seconde Timeline - certainement pas la dernière - comprenant les enrichissements suivants :

* Notre échantillon s'est élargi et précisé : les items non classés ont désormais réintégré leur rubrique, et l'archive de Remue est un peu plus détaillée, bien qu'encore incomplète. La timeline comprend aussi désormais _Climax_, un récit signé général Instin, publié en format papier aux [Éditions Le Nouvel Attila](http://www.lenouvelattila.fr). Une partie des archives du projet est en effet disponible en ligne sur un [site dédié, au nom du général](http://www.generalinstin.net).
* Nous avons ajouté des éléments graphiques - en l'occurrence les captures-écran de chaque item de la timeline. Sous forme de vignettes, ces captures offrent un aperçu de l'archive et, il faut bien le reconnaître, donnent à notre timeline un rendu déjà plus attractif... Les captures ont été réalisées grâce [à ce script Python](https://github.com/EcrituresNumeriques/corpusGI/blob/noTEI/screenshotGI.py), nous permettant de systématiser l'acquisition d'une nouvelle ressource image associée à chaque item de l'archive. Outre la visualisation dans cette chronologie, nous anticipons d'ors-et-déjà un usage des captures lors de l'éditorialisation à venir de l'archive.
* Le script de préparation des données [s'est enrichi et amélioré](https://github.com/EcrituresNumeriques/corpusGI/commit/82eb840ab991727064abc51a8bd9a23ad053e220), pour intégrer à la fois les nouvelles données et les évolutions successives du schéma de l'archive.

Voici le résultat :

<iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=16oIJKX4i2kYM-iSz7SfoGc0f7SZnbDvJ_oxzgGQ0mdI&font=Default&lang=fr&initial_zoom=2&height=650' width='150%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='1'></iframe>

## Origines et "Spin-off"

Si elle met un peu d'ordre dans notre archive, la Timeline du GI nous place aussi face à un nouveau défi : où commence le GI ? Où s'arrête-t-il (si seulement il s'arrête) ? Remue nous a servi jusqu'ici de référence (puisque le site a fait l'effort de recenser et de relier les différentes contributions au GI depuis 2005, pour en faire un véritable "feuilleton"), mais nous savons que l'aventure a commencé bien avant.

En 1997, Patrick Châtelier propose à ses compagnons du squat de la Grande aux Belles d'utiliser la photographie du vitrail ornant la tombe du Général (photo signée Juliette Soubrier), comme contrainte créative. Le Général fera dès lors une série d'apparitions ici et là, notamment dans la revue papier _Éponyme_. Ces publications, ces travaux, devront sans aucun doute être référencés dans notre Timeline. Mais Patrick Châtelier [le reconnait lui-même](http://remue.net/spip.php?article1521), la littérature s'est emparée d'Hinstin bien avant que le projet GI ne prenne forme : la famille Hinstin est ainsi mentionnée chez Lautréamont, Jarry, Kessel...

L'insertion de _Climax_ dans notre archive nous conduit par ailleurs à reconnaître l'autonomie relative de certains objets/projets reliés au GI : si _Climax_ reste une fiction collective, le petit groupe d'écrivains impliqué dans l'écriture ("petit groupe" au regard des dizaines de contributeurs recensés sur Remue) semble avoir été animé par un projet littéraire bien précis. La nature monographique de _Climax_, à cet égard, n'est probablement pas un hasard. Nous avons pour le moment décidé de qualifier de "spin-off" ces projets instiniens relativement autonomes - dont ferait partie aussi la traduction _Spoon River_.

Bref, notre timeline commence à prendre des proportions quelque peu monstrueuses... Probablement devrons-nous envisager de dessiner des modèles parallèles, pour éviter toute cacophonie. Cette absence relative de frontières est de toute façon l'une des spécifités du GI qui, comme tout objet éditorialisé, ne connaît pas de limite spatiale ni temporelle - ou comment le défi (l'échec ?) annoncé de notre tentative d'archivage, vient confirmer notre concept de départ...
