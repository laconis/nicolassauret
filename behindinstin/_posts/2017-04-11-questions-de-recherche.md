---
layout: post
title: Questions de recherche
date: '2017-04-11 15:06'
category: behindinstin
---

Au terme de trois mois de travail - principalement occupés par la constitution d'un échantillon de corpus suffisamment riche pour réaliser nos premières visualisations - nous commençons à nous familiariser avec le Général. Nous comprenons un peu mieux le projet qui anime la communauté GI - et notre propre recherche peut du coup se préciser.

En particulier, nous sommes parvenus à formuler plus clairement nos principales problématiques et à définir une première stratégie pour tenter de les résoudre.

## Qu'est ce que la littérature aujourd'hui? (question intermédiale)
Notre intérêt pour le projet Général Instin provient d'une intuition : Instin serait un cas d'étude idéal pour comprendre les formes et les pratiques littéraires contemporaines. La multiplicité comme l'hétérogénéité des médias et des formes d'expression croisés qui sont à l’œuvre dans le projet semblent suggérer un nouveau mode de publication littéraire (le terme "publication" étant compris au sens de "rendre public"). L'écriture littéraire, en ce sens, se conçoit autant à travers une communauté de contributeurs qu'une collection de médias. L'acte de publication, s'il perd une part de l'autorité que lui conférait les institutions littéraires, regagne en vitalité, en diversité et en circulation. La publication se libère de son carcan institutionnel, s'autonomise par rapport à lui et _s'autorise_. La mise en circulation se suffit à elle-même, valorisée par les nouvelles dynamiques qu'elle génère, jusqu'à ré-irriguer au passage les circuits traditionnels comme on peut le voir avec les "spins-of" de GI.

Deux objectifs de travail se dessinent dans l'immédiat :

1. interroger les médias utilisés, en commençant par les recenser, les classer et les analyser. Instin est-il seulement un objet littéraire - ou est-ce plutôt la catégorie même de "littéraire" qu'il faut repenser, en la réinscrivant d'ailleurs dans une histoire et une culture esthétique hors de l'imprimé ? On se doit en particulier de relever l'importance accordée à l'oralité : Instin fait l'objet de nombreuses lectures publiques archivées sur _Remue_[^lectures]. Cette forme d'expression, qui s'inscrit notamment dans le registre de la performance, ne doit certainement rien au hasard, aussi faudra-t-il l'interroger plus avant. Les arts visuels jouent eux aussi un rôle majeur : bien évidemment, la photographie du vitrail à l'origine du projet est abondamment reprise et retravaillée dans les publications. Mais d'autres genres ou d'autres tendances esthétiques sont aussi très présentes - ainsi la photo de famille, ou encore le _Land Art_ et ses œuvres éphémères (elles aussi caractérisées par un aspect performatif). Ces formes d'expression médiatique choisies ont, croyons-nous, une fonction et une signification profonde dans la construction du projet.
2. déterminer les dynamiques intermédiales. Puisque ces formes de publications sont en soit significatives, il reste à identifier leurs relations, leurs échanges, leurs tensions. Par exemple, la photographie du vitrail si souvent reprise participe d'une identité graphique propre au GI.

  ![Vitrail sur la tombe du Général Hinstin](http://i.imgur.com/RjS0xMI.jpg)

  Cette identité graphique, peut-elle être mise en lien avec une métaphore littéraire à l’œuvre ? En d'autres termes, est-il possible d'établir une relation de continuité entre un pattern textuel et un pattern visuel (ou inversement) ? Démontrer l'existence de cette écriture intermédiale - par laquelle l'image génère le récit qui, en retour, réécrit l'image - nous permettrait de comprendre comment se construit le GI et comment se fédère la communauté GI (alimentant par ce biais notre réflexion sur ce qu'est l'écriture contributive). Invoquer l'intermédialité semble particulièrement pertinent pour un objet comme Instin, qui ne semble exister qu'entre ses manifestations. Ces manifestations que nous tentons d'archiver consistent finalement en des intersections d'idées, d'acteurs, d'initiatives, dont Instin se pose en vecteur davantage qu'en œuvre. Il y a là tous les ingrédients d'un objet intermédial, qui ne se laisse appréhender qu'en faisant un pas de côté, un déplacement ontologique consistant à considérer les formes d'existence à partir des relations. Instin de ce point de vue se joue à l'_inter_, tant entre les médias utilisés, comme ce dialogue entre texte, visuel et parole, et bien sûr entre ses contributions ou manifestations.

[^lectures]: voir par exemple [Instin cherche Objet](http://remue.net/spip.php?article3320) ou [l’Électroencéphallusgramme](http://remue.net/spip.php?article3108).

### Méthodologie

* recenser les médias
* les classer : typologie (forme, thématique, rapport au littéraire)
* identifier des motifs, des récurrences, des évolutions
* identifier la dynamique à l’œuvre entre les médias

## Pourquoi le général Instin ? (question littéraire et métaphysique...)
Le Général (H)instin est bien plus qu'un prétexte à l'écriture collective. Pour être ainsi parvenu à fédérer autant de contributeurs, depuis près de 20 ans, c'est qu'il en appelle à des aspects particulièrement sensibles de notre imaginaire et de notre mémoire collective. La question "pourquoi le général ?" peut être comprise doublement. Tout d'abord, quelle est la symbolique du général ? N'y a-t-il pas là une figure martiale et autoritaire un peu contradictoire avec la forme ouverte (anarchique?) du GI ? Ou bien, justement, vient-on jouer de cet imaginaire et détourner la figure : «tous en ordre derrière le général»?

Ensuite, à quoi nous renvoie la figure historique d'Hinstin - général totalement passé aux oubliettes, et mort sans laisser de descendance directe ? Que vient, enfin, évoquer ce H perdu ? Enfin, comment s'articule ces deux aspects, à la frontière entre réel et imaginaire ?

![Capture sur le profil Facebook du GI](http://i.imgur.com/g0tznWi.png)

### Méthodologie

* explorer les champs sémantiques : comparer les textes (topic map), voir l'évolution dans le temps des thématiques investies par les contributeurs,
* comprendre comment le Général s'est transformé en objet littéraire, et comment il s'est disséminé (notre Timeline répond en partie à cet objectif).


## Comment agit l'environnement-dispositif ? (question communication)
L'absence supposée de dispositif dans Instin permet justement de poser, en creux, la question du dispositif. De même qu'Instin constitue intuitivement un cas d'étude idéal de littérature contemporaine, il pourrait nous éclairer sur la nature d'un environnement-dispositif ouvert à toute forme d'appropriation et de réécriture du Général. Pourtant, assez vite, on se rend compte que non, finalement même Instin n'y échappe pas au(x) dispositif(s), même si un méta-discours perpétue l'idée de l'ouverture et de l'objet insaisissable. Car, nos premiers pas dans la constitution de l'archive GI met à jour des lieux de cristallisation, des acteurs à la centralité plus ou moins prégnante, des formes, elles-mêmes investies de leur dispositif. Et ainsi, ces différentes manifestations produisent ensemble un archipel dont les contours semblent relativement identifiables, et dont la fonction peut être qualifiée de dispositive. Il n'y a certes pas de dispositif formel Général Instin, mais Instin produit un environnement _dispositif_ dans lequel les actions sont autant d'écritures dites _dispositives_. Une réflexion encore à l'état d'hypothèse, qu'il faut maintenant poursuivre, et qui viendra sans doute éclairer et nourrir [celle sur l'intermédialité], et d'une existence aux interstices.

[celle sur l'intermédialité]: #quest-ce-que-la-littrature-aujourdhui-question-intermdiale

### Méthodologie

* produire une.des cartographie.s des manifestations et des acteurs pour en identifier les nœuds et leur centralité : identifier les dynamiques d'autorité à l’œuvre pour éclairer la nature de l'environnement et les modalités qui en découle (et dont découlent les dynamiques d'autorité, simples projections de l'environnement dispositif),
* caractériser et théoriser cet environnement, "ce qui fait tenir l'archipel". Il s'agit de le mettre en regard de "dispositifs d'écriture" bien identifiés, le définir en creux par rapport aux objets connus,
* archiver Instin et maintenir le regard critique sur l'archive et son éditorialisation, tant la tension entre l'archive et son objet semble pouvoir nous éclairer sur la nature de l'objet. Notre démarche presque paradoxale ouvre des pistes de réflexion à la fois sur la question première, celle de la littérature contemporaine, et sur ce qui la rend possible, à savoir un dispositif de nature environnemental.

## Trois volets comme trois angles d'une même question

Ces trois volets de notre étude s'articulent pour mettre à jour trois aspects caractéristiques d'Instin :

1. **sa forme**, à partir de la question intermédiale
2. **ses modalités**, à partir de la question du dispositif
3. **son sujet**, à partir de la question métaphysique.

Il n'est pas certain que l'on arrive à en épuiser tous les aspects, aussi en viendrons-nous peut-être à ouvrir le chantier à la collaboration. En attendant, la méthode reste reproductible. C'est l'objet de ce carnet.
