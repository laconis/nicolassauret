---
layout: page
title: Behind Anarchy
---

Le carnet _Behind Anarchy_ présente nos recherches sur l'expérience transmédia [Anarchy.fr](http://anarchy.nouvelles-ecritures.francetv.fr/) de France Télévisions. Ce carnet a été animé par Ariane Mayer et Nicolas Sauret.

**Ariane Mayer** est maîtresse de conférences en littérature française à la Sorbonne-Nouvelle (UMR THALIM) et à l'IUT de Paris (département informatique). Sa thèse de doctorat, soutenue en 2016, portait sur les nouvelles formes de récits et de lectures littéraires portées par les technologies numériques. Elle continue depuis d'interroger la créativité et les pratiques littéraires à l'âge des écrans, tout en les mettant en relation avec les avant-gardes du XX siècle.




### About Anarchy

*Anarchy* est une fiction transmédia participative, proposée par France 4 et les [Nouvelles Ecritures de France Télévisions]. Produite par [Tel France Series], l'expérience a duré 6 semaines, du 29 octobre au 18
 décembre 2014.

Premier projet de création collective lancé à grande échelle par un média français, Anarchy.fr se définit comme une expérience littéraire collaborative propre aux nouvelles formes d'écriture numérique.



[Nouvelles Ecritures de France Télévisions]:http://nouvelles-ecritures.francetv.fr/
[Tel France Series]:http://www.telfrance.fr/societes/telfrance-serie/
[IRI]:http://www.iri.centrepompidou.fr
[nicolas]:http://nicolassauret.net


### Carnet

<div class="posts">
  <ul class="post">
  {% for post in site.categories.behindanarchy %}
      <li>
      <a href="{{ post.url }}">
        {{ post.title }}
      </a>
    <span class="post-date">{{ post.date | date_to_string }}</span>
    </li>
    <!-- {{ post.content }} -->
  {% endfor %}
  </ul>
</div>
