---
layout: post
title: Vers une méthode générique ?
date: '2016-08-26 14:59'
auteur: Nicolas
---

L'article et la communication au colloque Miau 2016 sont le résultat d'une triple analyse du corpus Anarchy. Nous détaillons ici la méthodologie utilisée à des fins de discussion, tel que nous l'avons proposé pour l'appel à communication [Écrire-éditer-lire à l'ère numérique][AAC].

## Une triple approche méthodologique

La nature du corpus nous a conduit à mener dans un premier temps une double approche quantitative et qualitative, pour tenter de dégager les modalités de construction et de manifestation de l'autorité dans un tel dispositif de production littéraire collaborative.

### Approche quantitative
L'approche quantitative s'est principalement portée sur l'analyse du réseau de personnages, généré par les mentions[^1] entre personnages. Cette approche quantitative relève donc d'une _analyse topologique_[^2] du réseau de personnages, mettant en évidence les proximités et les distances entre personnages, les communautés de personnages, les collaborations proches, ainsi que les stratégies de jeu de certains joueurs.

[^2]: Nous avons pour cela construit une matrice comptabilisant pour chaque personnage le nombre de fois qu'il mentionne un autre personnage. Cette matrice a ensuite été le point de départ de l'analyse topologique proprement dite à l'aide du logiciel d'analyse de graphe Gephi.  

[^1]: Pour rappel, ces mentions étaient directement intégrées à l'intérieur des contributions littéraires, sur le modèle des mentions dans un tweet par exemple. Les règles du jeu de l'univers Anarchy précisent qu'une mention entre personnages est possible (ou effective) que si les auteurs sont préalablement "associés". Mentionner d'autres personnages dans sa contribution a plusieurs effets : 1) envoi d'une notification à l'auteur dont le personnage est mentionné, 2) points supplémentaires attribués à l'auteur de la contribution si le personnage mentionné accepte/valide la contribution ou s'il ne rejette pas la contribution avant minuit, et enfin 3) double publication de la contribution sur les murs du personnage-auteur et du personnage mentionné.

### Approche qualitative
L'approche qualitative a consisté à étudier le corpus d'un point de vue littéraire. Cette _analyse littéraire_ s'est donc basée sur la lecture des contributions de l'échantillon et sur une compréhension fine des éléments narratifs et des interactions entre personnages[^3] dans le but d'identifier les jeux d'influence et d'autorité entre auteurs.

[^3]: Voir notamment la [typologie des mentions]({{ site.github.url }}/2016/03/16/chronologie-de-la-crete.html#typologie-des-mentions).

Ces deux approches combinées nous ont permis de mettre en évidence ce qu'on a appelé une autorité topologique, et une autorité narrative.

### Approche dispositive
Mais au cours de notre recherche, nous nous sommes rendu compte qu'une troisième approche était nécessaire pour valider l'hypothèse émergente d'une troisième composante de l'autorité, matérialisée par le dispositif transmédia régissant l'univers Anarchy. Nous avons alors complété les deux premières approches qualitative et quantitative par une approche dispositive, ou _organisationnelle_, consistant à décrire puis à critiquer le dispositif transmédia Anarchy. Le résultat de cette troisème analyse est ce qu'on a appelé l'_autorité dispositive_, étant comprise comme _"la série de contraintes générées par le dispositif"_ et qui _prédispose_ son usage.

Le tableau suivant permet de résumer notre triple approche.

<table class="tg8">
<tr>
<th class="tg-6l08">1</th>
<th class="tg-6l08">2</th>
<th class="tg-6l08">3</th>
</tr>
<tr>
<td class="tg-6l08">Approche quantitative<br><i class="fa fa-arrow-down"></i></td>
<td class="tg-6l09">Approche qualitative<br><i class="fa fa-arrow-down"></i></td>
<td class="tg-6l08">Approche organisationelle<br><i class="fa fa-arrow-down"></i></td>
</tr>
<tr>
<td class="tg-6l08">analyse topologique<br><i class="fa fa-arrow-down"></i></td>
<td class="tg-6l09">analyse littéraire<br><i class="fa fa-arrow-down"></i></td>
<td class="tg-6l08">analyse dispositive<br><i class="fa fa-arrow-down"></i></td>
</tr>
<tr>
<td class="tg-6l08">autorité topologique</td>
<td class="tg-6l09">autorité narrative</td>
<td class="tg-6l08">autorité dispositive</td>
</tr>
</table>

Les résultats de ces trois analyses sont à retrouver dans les publications à venir, ainsi que dans les communications au [colloque Miau]({{ site.github.url }}/2016/03/16/intervention-au-colloque-mediations-informatisees-de-l-autorite.html) et au [colloque Editorialisation de l'auteur]({{ site.github.url }}/2016/05/24/intervention-au-colloque-editorialisation-de-l-auteur.html)

## Une méthode générique ?

Outre les conclusions de notre étude sur les constructions de l'autorité dans les dispositifs d'écriture collaborative, nous nous interrogeons sur la méthodologie elle-même, sur sa validité ou sa reproductibilité.

La spécificité de la méthode est de compléter une double approche quanti-qualitative par une troisième analyse systématique des modalités de production et des conditions-mêmes de possibilités du corpus. Notre intuition, qui reste à vérifier sur d'autres corpus, est que cette triple approche pourrait être formalisée afin de proposer une méthode générique pour une analyse critique des écrits d'écran et plus largement de toute ressources nativement numériques, propre aux corpus des humanités numériques.

Selon la nature du corpus, l'analyse qualitative pourrait ainsi relever de l'analyse statistique, dans la mesure où tous les éléments d'un corpus ne sont pas nécessairement représentable dans un graphe relationnel. De même, l'analyse dispositive dépendrait d'une part de la nature du dispositif de production du corpus, et d'autre part des hypothèses de départ, mais resterait pertinente dès que l'on considèrerait des corpus nativement numériques. Et enfin, l'approche qualitative dépendrait de la discipline académique mobilisée, et pourrait donc relever de l'analyse littéraire, historique, anthropologique, etc.

Finalement, la méthode généricisée pourrait être résumée dans ce tableau :

<table class="tg8">
  <tr>
    <th class="tg-6l08">1</th>
    <th class="tg-6l08">2</th>
    <th class="tg-6l08">3</th>
  </tr>
  <tr>
    <td class="tg-6l08">Approche quantitative<br><i class="fa fa-arrow-down"></i></td>
    <td class="tg-6l09">Approche qualitative<br><i class="fa fa-arrow-down"></i></td>
    <td class="tg-6l08">Approche organisationelle<br><i class="fa fa-arrow-down"></i></td>
  </tr>
  <tr>
    <td class="tg-6l08">analyse statistique,<br>topologique, etc.</td>
    <td class="tg-6l09">analyse disciplinaire:<br>historique, littéraire, etc.</td>
    <td class="tg-6l08">analyse dispositive<br></td>
  </tr>
</table>

Comme je l'écrivais dans ma proposition au colloque, cette triple analyse réintroduit la matérialité du dispositif[^4] dans l'étude des corpus et ouvre ainsi la voie à une approche par le design pour la conception de nouveaux dispositifs d'éditorialisation, dans la perspective de ce que Jeffrey Schnapps a appelé le _Knowledge Design_ ou _le design de la connaissance_[^5]. Il s'agit bien là d'appréhender par le design cette nouvelle _episteme_ dont témoigne la mutation des objets paradigmatiques de la connaissance[^6], à savoir pour ne citer qu'eux&nbsp;: l'archive, le corpus de recherche, ou encore, le livre.

Cette proposition de généricisation de notre méthodologie reste encore entièrement à valider. Ce billet est une invitation à l'expérimentation et à la discussion. Pour démarrer cette dernière, ouvrez l'outil d'annotation en haut à droite de la page.


[^4]: Voir le billet de Marc Jahjah dans ce sens : _Digital et Book Studies (1/3) : le pari de la matérialité_. [http://marginalia.hypotheses.org/25252](http://marginalia.hypotheses.org/25252)
[^5]: Voir Schnapp, J. (2013, May). _Knowledge Design Incubating new knowledge forms / genres / spaces in the laboratory of the digital humanities._ Lecture presented at the Herrenhausen Conference “(Digital) Humanities Revisited – Challenges and Opportunities in the Digital Age”.
[^6]: Voir Gras, S.-E. (2016). _Les déplacements numériques des sciences humaines : un moment épistémologique ?_ In Le Tournant numérique des sciences humaines et sociales. Publications de la Maison des sciences de l'homme d'Aquitaine.

[AAC]:http://calenda.org/351910
