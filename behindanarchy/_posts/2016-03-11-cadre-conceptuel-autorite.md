---
layout: post
title: Cadre conceptuel, l'autorité dans un environnement anarchyque
date: '2016-03-11 15:47'
author: Ariane
---

Quelques réflexions sur la notion d'autorité, fréquemment utilisée dans les champs politique et informationnel, et que nous avons appliquée à un corpus littéraire.

## Un peu d'ordre dans l'autorité

S’il s’agit d’étudier «&nbsp;l’autorité dans _Anarchy_&nbsp;», une première difficulté se présente aussitôt. Comment peut-on appliquer la notion complexe d’autorité, fréquemment utilisée dans les champs politique et informationnel, à un corpus littéraire&nbsp;? Y a-t-il une pertinence à «&nbsp;sortir&nbsp;» ce concept de son cadre habituel pour lui permettre d’investir la sphère de la production artistique&nbsp;? Cette question exige d’abord de clarifier l’idée d’autorité ainsi que ses distinctions avec des termes connexes.

On sait que la notion d’autorité a fait l’objet de très nombreux travaux, depuis les réflexions de Max Weber jusqu’à la littérature actuelle qui s’intéresse à son évolution dans l’espace numérique (Merzeau, 2015, Vitali-Rosati, 2016). Quelques jalons historiques (qui ne visent aucunement l’exhaustivité) nous aideront à tracer quelques points de repères&nbsp;.

#### Weber et les sources de la domination légitime

On connaît la distinction, chez Weber, entre trois formes d’autorité (_Economie et société_, 1921). Pour le sociologue allemand ce qu’il nomme la « =&nbsp;domination légitime&nbsp;» se subdivise en trois types de sources ou de caractères&nbsp;:

-	«&nbsp;Un caractère **rationnel**, reposant sur la croyance en la légalité des règlements arrêtés et du droit de donner des directives qu’ont ceux qui sont appelés à exercer la domination par ces moyens (domination légale)&nbsp;;
-	Un caractère **traditionnel**, reposant sur la croyance quotidienne en la sainteté des traditions verbales de tout temps et en la légitimité de ceux qui sont appelés à exercer l’autorité par ces moyens (domination traditionnelle)&nbsp;;
-	Un caractère **charismatique**, [reposant] sur la soumission extraordinaire au caractère sacré d’une personne, ou encore [émanant] d’ordres révélés ou émis par celle-ci (domination charismatique)&nbsp;».

Cette distinction interne permet de mettre en relief la complexité de la notion d’autorité, liée à la pluralité de ses origines ou facteurs de légitimation. Ainsi la domination légale repose-t-elle sur une assise statutaire (une fonction octroyée et reconnue par une institution)&nbsp;; la domination traditionnelle sur une croyance (dans la légitimité des traditions héritées du passé)&nbsp;; et la domination charismatique sur une assise personnelle (l’aura, le caractère).

Dans le cas d’_Anarchy_, l’autorité rationnelle ou légale pourrait s’incarner dans l’institution qu’est la Rédaction&nbsp;; et l’autorité charismatique dans la personne des auteurs (à l’instar de N’Dish, on le verra plus tard). L’applicabilité de l’autorité traditionnelle à notre objet d’étude paraît plus problématique. Elle pourrait être constituée, soit par les règles du jeu explicitées par le mode d’emploi d’_Anarchy_ présent sur le site, soit par les règles implicites qui régissent le comportement des différents joueurs habitués à interagir en groupe, que ce soit ou non dans le cadre d’un dispositif spécifiquement numérique.

#### Hannah Arendt et la crise de l'autorité

Hannah Arendt mène une grande analyse de l’autorité dans _La Crise de la culture_, paru en 1961 (traduction française 1972). Pour elle, si la notion d’autorité implique toujours une forme d’obéissance, il ne faut pas pour autant la confondre avec la violence et la force. Pour la définir, Arendt commence par indiquer ce qu’elle n’est pas, c’est-à-dire ni la contrainte ni l’argument&nbsp;: «&nbsp;l’autorité exclut l’usage de moyens extérieurs de coercition&nbsp;; là où la force est employée, l’autorité proprement dite a échoué. L’autorité, d’autre part, est incompatible avec la persuasion qui présuppose l’égalité et opère par un processus d’argumentation&nbsp;». Autrement dit, précise-t-elle plus loin&nbsp;:

«&nbsp;S’il faut vraiment définir l’autorité, alors ce doit être en l’opposant à la fois à la contrainte par force et à la persuasion par arguments. (La relation autoritaire entre celui qui commande et celui qui obéit ne repose ni sur une raison commune, ni sur le pouvoir de celui qui commande ; ce qu’ils ont en commun, c’est la hiérarchie elle-même, dont chacun reconnaît la justesse et la légitimité, et où tous deux ont d’avance leur place fixée)&nbsp;».

Que pouvons-nous en déduire&nbsp;?

-	D’abord, Arendt rejoint Max Weber dans l’idée que l’autorité est une domination légitime, ou du moins, reconnue comme légitime par les membres qu’elle implique. Elle suppose donc, sinon une confiance, du moins une forme de consentement.
-	Cette domination légitime n’en suppose par moins une inégalité structurelle, une verticalité qui se traduit par des rôles différentiés au sein d’une hiérarchie. C’est bien du fait de cette inégalité que l’autorité ne repose pas sur l’argumentation ou sur la voie de la persuasion rationnelle&nbsp;: car celle-ci mettrait finalement les deux acteurs sur un pied d’égalité.
-	Ce qu’il y a de commun dans toute forme d’autorité, c’est donc la structure verticale et hiérarchique elle-même, en ce qu’elle détermine les places respectives des acteurs en jeu, en se gardant à la fois d’opérer leur nivellement (persuasion) et l’annihilation forcée de l’un des deux (violence).

#### Foucault et la critique du pouvoir

Michel Foucault a consacré un texte intéressant à une notion proche de celle d’autorité&nbsp;: le pouvoir (_Le sujet et le pouvoir_, 1972). De cet essai, où il cherche à décrire «&nbsp;comment s’exerce le pouvoir&nbsp;», nous pouvons retenir plusieurs choses&nbsp;:

1 - Le pouvoir se définit comme «&nbsp;une action sur des actions&nbsp;». En effet, «&nbsp;ce qui définit une relation de pouvoir, c’est un mode d’action qui n’agit pas directement et immédiatement sur les autres, mais qui agit sur leur action propre&nbsp;». C’est ce qui la différentie notamment de la violence, qui, elle, agit sur des corps et sur des choses : «&nbsp;elle force, elle plie, elle brise, elle détruit&nbsp;; […] elle n’a donc auprès d’elle d’autre pôle que celui de la passivité&nbsp;»&nbsp;; là où la «&nbsp;relation de pouvoir, en revanche, s’articule sur deux éléments qui lui sont indispensables pour être justement une relation de pouvoir : que l’autre (celui sur lequel elle s’exerce) soit bien reconnu et maintenu jusqu’au bout comme sujet d’action&nbsp;; et que s’ouvre, devant la relation de pouvoir, tout un champ de réponses, réactions, effets, inventions possibles&nbsp;».

2 - Le pouvoir foucaldien, donc, comme l’autorité chez Hannah Arendt, tient ensemble l’obéissance et le maintien d’une certaine forme de liberté. Comme pour Arendt, cette liberté ne signifie pas pour autant que les acteurs en jeu soient sur un pied d’égalité et que l’un convainque l’autre par des arguments du bien-fondé de ce qu’il lui demande. Foucault distingue le pouvoir aussi bien de la contrainte externe que du consentement&nbsp;: «&nbsp;il n’est pas en lui-même renonciation à une liberté&nbsp;» ; mais il n’est pas non plus «&nbsp;dans sa nature propre la manifestation d’un consensus&nbsp;». Ainsi le pouvoir, sans niveler les deux partenaires qu’il implique, présuppose cependant toujours la liberté : il «&nbsp;ne s’exerce que sur des sujets libres, et en tant qu’ils sont libres&nbsp;», et «&nbsp;l’esclavage n’est pas un rapport de pouvoir lorsque l’homme est aux fers (il s’agit alors d’un rapport physique de contrainte)&nbsp;».

3 - Cependant, là où l’autorité telle que l’analyse Arendt s’exerce principalement sur des personnes, en revanche le pouvoir s’applique à un champ plus large d’objets. Il peut en effet, selon la distinction de Foucault, s’exercer sur&nbsp;:
- Des **choses**&nbsp;: il donne alors «&nbsp;la capacité de les modifier, de les utiliser, de les consommer ou de les détruire – un pouvoir qui renvoie à des aptitudes directement inscrites dans le corps ou médiatisées par des relais instrumentaux&nbsp;»&nbsp;: il s’agit alors de «&nbsp;capacité&nbsp;» ;
- Des **signes**&nbsp;: ce sont alors «&nbsp;les relations de pouvoir des rapports de communication qui transmettent une information à travers une langue, un système de signe ou tout autre médium symbolique. Sans doute communiquer, c’est toujours une certaine manière d’agir sur l’autre ou les autres&nbsp;» ;
- Et des **individus** (ou des groupes)&nbsp;: «&nbsp;si on parle du pouvoir des lois, des institutions ou des idéologies, si on parle de structures ou de mécanismes de pouvoir, c’est dans le mesure seulement où on suppose que certains exercent un pouvoir sur d’autres&nbsp;»&nbsp;; le terme de pouvoir désigne alors «&nbsp;des relations entre partenaires&nbsp;».

Il est intéressant de noter que, si Foucault distingue ces différents types de pouvoirs, il montre cependant que tous sont étroitement reliés&nbsp;: «&nbsp;relations de pouvoir, rapport de communication, capacité objectives ne doivent donc pas être confondus. Ce qui ne veut pas dire qu’il s’agisse de trois domaines séparés[…]. Il s’agit de trois types de relations qui, de fait, sont toujours imbriquées les unes dans les autres, se donnant un appui réciproque et se servant mutuellement d’instrument&nbsp;».

4 - Enfin, Foucault émet tout comme Hannah Arendt une réflexion sur le devenir de cette notion dans le monde qui lui est contemporain. Seulement, là où Arendt parle de la «&nbsp;crise&nbsp;» et de la «&nbsp;disparition&nbsp;» de l’autorité dans le monde moderne, Foucault évoque plutôt les luttes contemporaines contre les différentes formes de domination, d’exploitation et d’assujettissement – luttes qui présupposent justement que les relations de pouvoir, pour leur part, n’ont pas «&nbsp;disparu&nbsp;». Ainsi prend-il pour exemple et pour point de départ de sa réflexion «&nbsp;une série d’oppositions qui se sont développées ces quelques dernières années&nbsp;: l’opposition au pouvoir des hommes sur les femmes, des parents sur leurs enfants, de la psychiatrie sur les malades mentaux, de la médecine sur la population, de l’administration sur la manière dont les gens vivent&nbsp;».

#### Les réseaux numériques et les mesures de l'influence

Outre la domination légitime (Weber), l’autorité (Arendt) et le pouvoir (Foucault), un autre concept proche est celui d’influence, tel qu’il a été particulièrement développé à propos des dynamiques et des structurations de l’espace numérique, en particulier avec le web social. C’est le cas par exemple de travaux comme «&nbsp;Everyone’s an Influencer&nbsp;: Quantifying influence on Twitter&nbsp;» (Eytan Bakshy, Jake M. Hofman, Winter A. Mason et Duncan J. Watts) ou «&nbsp;Measuring User Influence on Twitter&nbsp;: the Million Follower Fallacy&nbsp;» (Meeyoung Cha, Hamed Haddadi, Fabricio Benevenuto, Krishna P. Gummadi) au sujet de Twitter. Nous pouvons retenir de ces recherches (notamment celle de Cha _et al._) :

1 - Leur définition de l’influence comme «&nbsp;pouvoir ou capacité de provoquer un effet de manière indirecte ou intangible&nbsp;», que les auteurs reformulent ainsi par la suite&nbsp;: l’influence est le «&nbsp;potentiel qu’a un individu de conduire les autres à faire certaines actions&nbsp;». De manière ainsi indéfinie (on ignore plus précisément quel sens donner à ces adjectifs «&nbsp;indirect&nbsp;» et «&nbsp;intangible&nbsp;», la notion d’influence semble entretenir un certain lien avec celle de pouvoir chez Foucault, défini comme «&nbsp;un ensemble d’actions sur des actions possibles&nbsp;: il opère sur le champ de possibilité où vient s’inscrire le comportement de sujets agissants&nbsp;: il incite, il induit, il détourne, il limite, il rend plus ou moins probable&nbsp;; à la limite, il contraint ou empêche absolument&nbsp;; mais il est bien toujours une manière d’agir sur un ou sur des sujets agissants, et ce en tant qu’ils agissent ou qu’ils sont susceptibles d’agir. Une action sur des actions&nbsp;». Peut-être seulement que là où le pouvoir foucaldien consiste à «&nbsp;conduire des conduites&nbsp;», l’influence ainsi caractérisée consisterait pour sa part à induire des conduites.

2 - L’influence telle qu’elle est étudiée sur des plateformes de micro-blogging comme Twitter présente souvent un aspect quantitatif&nbsp;: il ne s’agit pas, comme le fait Foucault, de décrire comment cela se passe ou comment elle s’exerce&nbsp;; mais plutôt d’envisager des métriques qui permettent de l’identifier. Chez Cha _et alii_ par exemple, on distingue trois métriques de l’influence d’un utilisateur&nbsp;:
- son nombre de followers (qui indique la taille de son audience),
- le nombre de posts qu’il relaie (la «&nbsp;retweet influence&nbsp;») et
- le nombre d’autres utilisateurs qui mentionnent son nom dans leurs propres posts (la «&nbsp;mention influence&nbsp;»).

Qu’est-ce que les notions de domination légitime chez Weber, d’autorité chez Arendt, de pouvoir chez Foucault et d’influence à l’ère numérique ont de commun&nbsp;? Elles supposent toutes une relation verticale et hiérarchisée entre deux ou plusieurs personnes, dont l’efficacité coercitive ne repose ni sur des moyens externes (la force) ni sur ce que les personnes en jeu ont de commun (la raison), mais sur la hiérarchie elle-même quelle qu’en soit la provenance (institutionnelle, personnelle, traditionnelle, sociale ou encore liée à un degré d’action et de participation sur un réseau). La seule chose qui semble unifier ces différents concepts, c’est en effet l’existence d’une relation dissymétrique, où l’un des membres agit sur les actions du second, que ce soit de façon directive ou indirecte.

## L'autorité en toutes lettres

#### Quelles luttes de pouvoir&nbsp;?

Comment, pour notre part, appliquer ce champ lexical diffus de l’autorité à la spécificité d’un corpus littéraire&nbsp;? Il se trouve qu’on est en présence de deux relations dissymétriques&nbsp;:

-	Celle, statutaire et _de droit_, qui sépare la Rédaction (incarnation d’une autorité institutionnelle) des auteurs-joueurs (sur lesquels s’exerce cette autorité, en tant qu’ils doivent se plier aux règles du jeu). Les rapports de force entre la Rédaction et les auteurs semble pouvoir être mesurés par le fil d’actualité tenu par la Rédaction, qui intègre ou relaie plus ou moins les actions narratives des auteurs&nbsp;; ainsi que par le pouvoir de distorsion et de détournement que les joueurs peuvent exercer sur les règles du jeu et le dispositif lui-même.
-	Celle, _de fait_, qui sépare les auteurs-joueurs entre eux, puisque ceux-ci se caractérisent par différents pouvoirs d’infléchissement des actions des autres joueurs (à travers la mention du personnage d’un autre joueur, on peut l’inciter à une action narrative ou à suivre une action de sa propre initiative). Ces rapports de force entre les joueurs, où chacun vise pour sortir «&nbsp;gagnant&nbsp;» à accroître sa propre influence sur le récit, peuvent être mesurés par divers indicateurs qu’il s’agira d’éclaircir.   

En somme, on se trouve avec _Anarchy_ face à deux types de luttes&nbsp;: une lutte de pouvoir qui oppose les joueurs et la Rédaction&nbsp;; et une lutte d’influence qui met en compétition les joueurs entre eux.

<img title="Circulation de l'influence" src="{{ site.baseurl }}/assets/img/circulation_influence.jpg" class="img-thumbnail"  style="max-width:60%;margin-left:20%">

Figure : _Circulation de l'influence dans Anarchy_
{: class="figurelegende"}

#### L'autorité comme circulation

Pour mesurer l’une et l’autre dans le cadre d’une fiction littéraire collaborative, il faut donc adapter notre objet d’étude, l’autorité, aux spécificités de notre corpus. C’est-à-dire surtout à deux de ses particularités&nbsp;:

-	Son caractère **dynamique** (l’autorité de chaque joueur, ainsi que son influence sur les autres et sur le récit, est une construction qui n’est pas stable mais peut varier au cours du temps : il convient donc d’envisager le caractère processuel de l’autorité)&nbsp;;
-	Son caractère **stratégique** (dans le cadre d’un jeu, où le but est de «&nbsp;gagner&nbsp;» en augmentant au maximum son nombre de points, chaque joueur déploie un certain nombre de tactiques qui lui permet d’influencer le plus possible le cours du récit des autres et celui de l’arche narrative dessinée par la Rédaction).

On a choisi d’envisager ces stratégies comme une **circulation** de l’autorité qui repose sur un travail par lequel chaque joueur tente d’accroître sa visibilité, sa notoriété et sa légitimité. On entendra ainsi la distinction entre ces termes proches&nbsp;: la **visibilité** d’un joueur consiste dans le fait d’être connu du public en occupant fortement l’espace médiatique&nbsp;; sa **notoriété** dans le fait d’être un nœud génératif de contenu reconnu par ses pairs, et sa **légitimité** dans le crédit que lui accorde la Rédaction. La différence entre ces notions peut ainsi se lire du point de vue à la fois de leur **relation** (la visibilité est une condition de possibilité de la notoriété, qui en est une pour la légitimité, qui en est une pour l’influence), de leur **statut** (visibilité, notoriété et légitimité sont des états de fait là où l’influence est une capacité), et de leur **cible** (le public, les pairs, la Rédaction).

La circulation de l’influence telle que la véhiculent ces trois stratégies peut être modélisée de la façon suivante&nbsp;:

<img title="Relations conceptuelles de l'influence" src="{{ site.baseurl }}/assets/img/relations_influence.png" class="img-thumbnail"  style="max-width:80%;margin-left:10%">

Figure : _Circulation de l'autorité_
{: class="figurelegende"}


C’est ce cadre conceptuel, permettant d’envisager les modes de construction de l’autorité dans le cas particulier d’un corpus littéraire, qu’on retiendra pour mesurer l’influence narrative du personnage de La Crête.
