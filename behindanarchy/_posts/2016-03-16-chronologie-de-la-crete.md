---
layout: post
title: Chronologie de La Crête
date: '2016-03-16 14:29'
author: Nicolas
---

Une première analyse qualitative sur le corpus Anarchy nous a amené à nous intéresser plus particulièrement [au cas La Crête]({{ post_url behindanarchy/2016-03-14-le-cas-la-crete }}) et à dresser une chronologie des contributions de La Crête.

Pour cela, nous avons isolé du corpus complet toutes les contributions écrites _par_ La Crête ou _à_ La Crête, c'est-à-dire le mentionnant : *@La Crête*.

Le tableau suivant résume cet échantillon (104 contributions sur 11280), ainsi qu'une brève analyse du registre de discours mobilisé par les auteurs. En effet les types de contributions varient d'un personnage à l'autre et parfois pour un même personnage d'une contribution à l'autre. Les posts sont le plus souvent narratifs, mais peuvent parfois aussi relever de l'injonction, de la proposition, voir sortir de la fiction.

<a class="btn btn-default btn-sm" href="{{ site.github.url}}/assets/data/tableau_chronologie_lacrete.csv" title="Download csv table">
<i class="fa fa-download"></i> Sauvegarder le tableau (csv)</a> |
<a class="btn btn-default btn-sm" href="#typologie-des-mentions" title="Typologie des mentions">
  Sauter le tableau<i class="fa fa-fast-forward"></i>
</a>

<style type="text/css">
  .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
  .tg td{font-size:0.6em;padding:5px 2px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;}
  .tg th{font-size:0.6em;font-weight:normal;padding:5px 2px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px;}
  .tg .tg-yw4l{vertical-align:top}
  .tg .tg-yw3l{background-color:bisque}
</style>
<table class="tg">
  <tr>
    <th class="tg-yw4l">ID</th>
    <th class="tg-yw4l">Date complète</th>
    <th class="tg-yw4l">Personnage</th>
    <th class="tg-yw4l">Auteur</th>
    <th class="tg-yw4l">Titre</th>
    <th class="tg-yw4l">Evénements</th>
    <th class="tg-yw4l">Mentions</th>
    <th class="tg-yw4l">Remarques</th>
  </tr>
  <tr>
    <td class="tg-yw3l">498</td>
    <td class="tg-yw3l">2014-10-30 23:54:04</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">L'éveil</td>
    <td class="tg-yw3l">Discute au troquet sur la difficulté de la crise;Tags sur la nécessité de l'entraide</td>
    <td class="tg-yw3l">0</td>
    <td class="tg-yw3l">Sur le mode du journal intime, sans adresse</td>
  </tr>
  <tr>
    <td class="tg-yw3l">811</td>
    <td class="tg-yw3l">2014-10-31 15:35:05</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">La tête dans le cul mais toujours au soleil !</td>
    <td class="tg-yw3l">Se réveille dans un squat,Rencontre une punk à chien et discute d'anarchisme et d'entraide</td>
    <td class="tg-yw3l">0</td>
    <td class="tg-yw3l">Sur le mode du journal intime, sans adresse</td>
  </tr>
  <tr>
    <td class="tg-yw4l">1658</td>
    <td class="tg-yw4l">2014-11-01 17:16:10</td>
    <td class="tg-yw4l">ed-stotzenberg</td>
    <td class="tg-yw4l">Belial</td>
    <td class="tg-yw4l">Le réseau ...</td>
    <td class="tg-yw4l">Ed essaie de constitue son réseau pour changer la situation: appelle Raoul Perez. Cite La Crête comme exemple de SDF.</td>
    <td class="tg-yw4l">Raoul Perez</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">2118</td>
    <td class="tg-yw4l">2014-11-02 20:21:33</td>
    <td class="tg-yw4l">capucine-cher</td>
    <td class="tg-yw4l">Belial</td>
    <td class="tg-yw4l">LOIC</td>
    <td class="tg-yw4l">Capucine décrit le plan LOIC pour changer la situation, qui implique un large réseau de rebelles dont La Crête.</td>
    <td class="tg-yw4l">La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">2174</td>
    <td class="tg-yw3l">2014-11-03 09:03:00</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">ça s'accélère....</td>
    <td class="tg-yw3l">Voit Isa, sa pseudo petite-amie;il apprend qu'Ed Stötzenberg veut le joindre par téléphone, pour "tout péter"""</td>
    <td class="tg-yw3l">Ed Stötzenberg</td>
    <td class="tg-yw3l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">2230</td>
    <td class="tg-yw3l">2014-11-03 14:55:12</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">La Dynamite</td>
    <td class="tg-yw3l">C'est en effet Ed Stötzenberg qui veut le joindre, qui lui envoie un poème révolutionnaire de René Binamé</td>
    <td class="tg-yw3l">Ed Stötzenberg</td>
    <td class="tg-yw3l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">2484</td>
    <td class="tg-yw4l">2014-11-04 08:15:26</td>
    <td class="tg-yw4l">ed-stotzenberg</td>
    <td class="tg-yw4l">Belial</td>
    <td class="tg-yw4l">Putain d'adrénaliiiiine !!</td>
    <td class="tg-yw4l">Ed réexplique son plan d'action et sa philosophie; indique aussi qu'il compte beaucoup sur La Crête.</td>
    <td class="tg-yw4l">La Crête</td>
    <td class="tg-yw4l">Premiers contacts sociaux</td>
  </tr>
  <tr>
    <td class="tg-yw3l">2892</td>
    <td class="tg-yw3l">2014-11-05 22:36:06</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Remember, remember, the 5th of november</td>
    <td class="tg-yw3l">Fait avec Isis le tour de ses amis SDF;prépare attentat sur la sous-préfecture de Briançon ("opération vendetta des 66"); Espère qu'Ed s'y joindra</td>
    <td class="tg-yw3l">Ed Stötzenberg</td>
    <td class="tg-yw3l">Se pose en force de proposition et preneur d'initiatives, d'autres le suivent</td>
  </tr>
  <tr>
    <td class="tg-yw4l">3476</td>
    <td class="tg-yw4l">2014-11-07 14:37:42</td>
    <td class="tg-yw4l">gilles-froid</td>
    <td class="tg-yw4l">Froid Family</td>
    <td class="tg-yw4l">Nour ou Isis ?</td>
    <td class="tg-yw4l">Gilles Froid raconte que trois blessés (de l'attentat du 5 novembre) sont chez Juan. L'un d'eux, James, est hostile et menace les autres: il envoie La Crête et Gilles Froid en mission pour récupérer son argent.</td>
    <td class="tg-yw4l">Juan; La Crête; Isis; James</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">4657</td>
    <td class="tg-yw4l">2014-11-11 03:59:25</td>
    <td class="tg-yw4l">juan</td>
    <td class="tg-yw4l">The Peacemaker</td>
    <td class="tg-yw4l">James</td>
    <td class="tg-yw4l">Juan demande des nouvelles à Gilles Froid et La Crête partis en expédition sous la menace de James; il raconte comment ça se passe chez lui avec Isis et Nicolas pour gérer et mater James</td>
    <td class="tg-yw4l">Gilles Froid; La Crête; James; Nicolas</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">4676</td>
    <td class="tg-yw4l">2014-11-11 12:52:20</td>
    <td class="tg-yw4l">gilles-froid</td>
    <td class="tg-yw4l">Froid Family</td>
    <td class="tg-yw4l">Anarchie, fric et botanique</td>
    <td class="tg-yw4l">Gilles Froid raconte son expédition avec La Crête pour trouver l'argent de James et ainsi libérer Isis, Juan et Nicolas de son joug</td>
    <td class="tg-yw4l">La Crête; Isis; Juan, Nicolas; James</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">4767</td>
    <td class="tg-yw3l">2014-11-12 04:32:53</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Putain de merde !</td>
    <td class="tg-yw3l">Part à Lyon avec Isis;bagarre avec un caïd;rencontre Juan et Gilles Froid (amis d'Isis),Puis Nour dans un squat</td>
    <td class="tg-yw3l">Juan; Gritusse; Gilles Froid; Jérôme Mariva</td>
    <td class="tg-yw3l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">4798</td>
    <td class="tg-yw3l">2014-11-12 12:29:43</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Le silence de mon attentat</td>
    <td class="tg-yw3l">Chez Juan, s'étonne du fait que son attentat du 5 novembre à Briançon n'ait pas été relayé par les médias;Demande à des amis de diffuser l'info</td>
    <td class="tg-yw3l">Boitro; Gritusse; FranceIndé; Liza Papanov; Victor; Tous Ensemble; EveillésManifeste; Charlito; Baratribord; Anonymous; France BN; Jérôme Mariva</td>
    <td class="tg-yw3l">Une sorte de méta-message qui permet à la Crête de faire davantage entendre sa voix et connaître ses actions : il est en train de construire lui-même sa visibilité en demandant aux autres de relayer les événements qu'il a provoqués</td>
  </tr>
  <tr>
    <td class="tg-yw3l">4801</td>
    <td class="tg-yw3l">2014-11-12 13:30:11</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Mon attentat passé sous silence</td>
    <td class="tg-yw3l">Même chose;demande à d'autres personnes de diffuser la nouvelle de son attentat du 5 novembre</td>
    <td class="tg-yw3l">Gilles Froid; Annie Botul; Alistair</td>
    <td class="tg-yw3l">Même chose, construit sa visibilité médiatique</td>
  </tr>
  <tr>
    <td class="tg-yw4l">4813</td>
    <td class="tg-yw4l">2014-11-12 14:51:49</td>
    <td class="tg-yw4l">france-bn</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Terrorisme sur le Territoire</td>
    <td class="tg-yw4l">France BN relaye l'info de l'attentat du 5 novembre par La Crête</td>
    <td class="tg-yw4l">La Crête</td>
    <td class="tg-yw4l">La construction de la visibilité fonctionne par sa diffusion dans la presse</td>
  </tr>
  <tr>
    <td class="tg-yw4l">4946</td>
    <td class="tg-yw4l">2014-11-12 20:39:05</td>
    <td class="tg-yw4l">france-bn</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Et si Philae avait détécté une vie extraterrestre ?</td>
    <td class="tg-yw4l">France BN évoque l'info de l'atterrissage de Philae; s'adresse à sa liste d'abonnés dont fait partie La Crête</td>
    <td class="tg-yw4l">Ed le Banquier; Ivan Papanov; La Crête; Johanna Mercier; FranceIndé; Gilles de Salm; Le Prophète; Camille Singedoigt; Kevin; Natalie; Marianne Guérin; Mathilde Pevensie</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">5041</td>
    <td class="tg-yw3l">2014-11-13 15:32:52</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Le Black Bloc</td>
    <td class="tg-yw3l">Toujours chez Juan;arrive à Paris bientôt;Veut contacter ses amis là-bas pour faire" tout péter"""</td>
    <td class="tg-yw3l">Juan; Ingrid; Gritusse</td>
    <td class="tg-yw3l">Fait appel aux autres pour qu'il se joigne à lui; accroît son influence</td>
  </tr>
  <tr>
    <td class="tg-yw3l">5101</td>
    <td class="tg-yw3l">2014-11-13 22:01:57</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Le Testament de mon sentiment moral</td>
    <td class="tg-yw3l">Annonçant qu'il veut lutter de toutes ses forces contre le pouvoir, il adresse à ses amis un testament de ses idées morales et politiques</td>
    <td class="tg-yw3l">Baratribord; Gritusse; Erin; Jean Boitro; Sandrine; Victor; Liza Papanov; FranceIndé; LEON; EveillésManifeste; Ingrid; Charlito; Tous Ensemble; France BN; Anonymous; Capucine Cher; Alistair ou 'Ali'; Noé; Jerôme Mariva; Deadlock; Natalie; Tiphanie; Marianne Guérin</td>
    <td class="tg-yw3l">Le personnage fait connaître ses idées auprès d'un certain nombre d'interlocuteurs: continue à construire sa visibilité</td>
  </tr>
  <tr>
    <td class="tg-yw4l">5151</td>
    <td class="tg-yw4l">2014-11-13 19:05:41</td>
    <td class="tg-yw4l">deadlock</td>
    <td class="tg-yw4l">Mordem Frost</td>
    <td class="tg-yw4l">Eveillons-nous</td>
    <td class="tg-yw4l">Deadlock cherche à relayer encore plus l'info de l'attentat du 5 novembre qui a été censurée; s'adresse à une centaine de personnages</td>
    <td class="tg-yw4l">Anonymous; La Crête; @Renée-Guillemette @Jo @Costard rose @Pépette @Jacques @Tim @Michel Caravage @Chouquette @Chilon de Sparte @Bias de Priène @Baratribord @Gritusse @David @geva @Thomas  @Tiphanie @Daniel Noyez + plusieurs dizaines<br></td>
    <td class="tg-yw4l">Attestation et amplification de la visibilité du personnage de la Crête, témoigne de sa notoriété et de son influence grâce à une diffusion à grande ampleur de ses activités narratives</td>
  </tr>
  <tr>
    <td class="tg-yw4l">5212</td>
    <td class="tg-yw4l">2014-11-13 21:43:03</td>
    <td class="tg-yw4l">eveillesmanifeste</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Le grève continue</td>
    <td class="tg-yw4l">Manifeste des Eveillés, groupe de solidarité et de grève: fait appel à plusieurs dizaines de personnages pour faire circuler ce message, dont La Crête.</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">La Crête appelé comme porte-voix à même de diffuser un message</td>
  </tr>
  <tr>
    <td class="tg-yw3l">5224</td>
    <td class="tg-yw3l">2014-11-14 00:39:26</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">le Testament de mon sentiment moral</td>
    <td class="tg-yw3l">Même chose que plus haut: testament de ses idées morales avec adresses à d'autres pour le relayer / communiquer</td>
    <td class="tg-yw3l">@Gritusse  @Erin @Jean Boitro @Sandrine @Victor @Liza Papanov @FranceIndé @LEON @EveillésManifeste @Ingrid @Charlito @Tous Ensemble @France BN @Anonymous @Capucine Cher @Alistair ou 'Ali' @Noé @Jerôme Mariva @Deadlock @Natalie @Tiphanie @ Marianne Guérin</td>
    <td class="tg-yw3l">Même chose que plus haut (testament), adressé à davantage de personnages: La Crête se fait connaître par la citation d'interlocuteurs qui vont lire ses plans et ses pensées</td>
  </tr>
  <tr>
    <td class="tg-yw4l">5436</td>
    <td class="tg-yw4l">2014-11-14 13:06:05</td>
    <td class="tg-yw4l">gilles-froid</td>
    <td class="tg-yw4l">Froid Family</td>
    <td class="tg-yw4l">L'horreur</td>
    <td class="tg-yw4l">Gilles Froid met à distance ses soucis par rapport à James: un message de sa sœur lui indique que Clarisse est morte.</td>
    <td class="tg-yw4l">La Crête, Juan, Isis, Nicolas, James</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">5442</td>
    <td class="tg-yw4l">2014-11-14 14:41:14</td>
    <td class="tg-yw4l">juan</td>
    <td class="tg-yw4l">The Peacemaker</td>
    <td class="tg-yw4l">N'est pas Don Juan qui veut...</td>
    <td class="tg-yw4l">Juan se pose des questions sur son amitié ambigüe avec Isis; mais "rate" son moment avec elle. Ils sortent de l'appartement et voient Gilles Froid et La Crête.</td>
    <td class="tg-yw4l">James, Isis, Gilles Froid, La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">5471</td>
    <td class="tg-yw4l">2014-11-14 19:07:07</td>
    <td class="tg-yw4l">franceinde</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Dépêche</td>
    <td class="tg-yw4l">Diffuse infos sur les blessés recueillis par la Croix-Rouge au Camp des éveillés. Demande à certains personnages dans les manifs (dont la Crête) de transmettre des infos.</td>
    <td class="tg-yw4l">Lilou; Fantasio; Youri Gagarine; Renée-Guillemette; Tiphaine; Stelise; Jeremy; Ivan Papanov; La Crête; Charlito; Marie Dujardin</td>
    <td class="tg-yw4l">Visibilité, influence (par sa capacité à relayer l'information)</td>
  </tr>
  <tr>
    <td class="tg-yw4l">5812</td>
    <td class="tg-yw4l">2014-11-15 22:31:16</td>
    <td class="tg-yw4l">robert-mortimer</td>
    <td class="tg-yw4l">Franc fort? Comme la saucisse</td>
    <td class="tg-yw4l">The queen needs you</td>
    <td class="tg-yw4l">La reine d'Angleterre veut collaborer avec un certain nombre de personnes élites du pays, dont la Crête</td>
    <td class="tg-yw4l">Jean de Severac @Dr Lavigne Mathieu @DeparDieU Gérard @La Crête @Timothée @Robert BRASILLAC @Léon Perjols @LEON @Marilou @Ratatouille @TANGUY CRS 4587 @le Petit Nicolas @Conseiller bac+15@Camille Singedoigt @Olivier Dubois @Johanna Mercier  @Conseiller bac+15 @Paul Rigal @Marie Dujardin @Thierry Lun @Louisette Le Coz @Charles Dugalois</td>
    <td class="tg-yw4l">Atteste de visibilité et reconnaissance: considéré comme l'une des élites du pays, un élément dont les actions sont susceptibles d'influencer le cours des événements</td>
  </tr>
  <tr>
    <td class="tg-yw3l">6133</td>
    <td class="tg-yw3l">2014-11-17 00:20:34</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Les gens sont cons</td>
    <td class="tg-yw3l">Informe ses amis qu'il ne peut pas recevoir les mails, lettres, colis, appels qu'ils lui envoient car n'a pas d'adresse ni téléphone</td>
    <td class="tg-yw3l">Juan</td>
    <td class="tg-yw3l">Méta-message de l'auteur au sujet des moyens de le joindre</td>
  </tr>
  <tr>
    <td class="tg-yw4l">6408</td>
    <td class="tg-yw4l">2014-11-16 16:27:15</td>
    <td class="tg-yw4l">jerome-mariva</td>
    <td class="tg-yw4l">Mordem Frost</td>
    <td class="tg-yw4l">Action!</td>
    <td class="tg-yw4l">S'inquiète de la volonté de la Crête de "faire tout sauter". Espère qu'il s'engager auprès des volontaires suite à l'appel de Lilou.</td>
    <td class="tg-yw4l">La Crête; Noé; EveillésManifeste; Charlito; Lilou</td>
    <td class="tg-yw4l">D'autres personnages parlent de lui sans interagir avec lui de manière narrative ==&gt; commentaires sur le personnage lui-même, attestant de son influence</td>
  </tr>
  <tr>
    <td class="tg-yw4l">6428</td>
    <td class="tg-yw4l">2014-11-16 17:54:27</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">les Eveillés à la tête de Paris</td>
    <td class="tg-yw4l">Demande entre autres à la Crête de "faire passer le mot": les éveillés ont un plan pour prendre la tête de la mairie de Paris</td>
    <td class="tg-yw4l">Tiphanie; Stelise; Un Petit Rien; Jeremy; Victor; Auguste Baboeuf; Alex V.; Charlito; Capucine Cher; La Crête; Marianne Guérin; Conseiller bac + 15; justin tresor; Liza Papanov</td>
    <td class="tg-yw4l">La Crête appelé encore une fois comme relais d'information: visibilité et influence</td>
  </tr>
  <tr>
    <td class="tg-yw4l">6476</td>
    <td class="tg-yw4l">2014-11-17 16:18:38</td>
    <td class="tg-yw4l">jeanne-juanita</td>
    <td class="tg-yw4l">Franc fort? Comme la saucisse</td>
    <td class="tg-yw4l">Radio Monte Carlo</td>
    <td class="tg-yw4l">Ernesto rebaptise la chaîne de TV "Télé Révolution" et engage des "personnalités engagées" comme La Crête</td>
    <td class="tg-yw4l">@Antoll MA @Johanna Mercier  @Jacques Bismute @EveillésManifeste @La Crête @BPC @le Petit Nicolas @Ratatouille</td>
    <td class="tg-yw4l">Reconnaissance sociale du personnage comme engagé et influent</td>
  </tr>
  <tr>
    <td class="tg-yw3l">6789</td>
    <td class="tg-yw3l">2014-11-18 23:48:56</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Paname !</td>
    <td class="tg-yw3l">Se procure des armes auprès d'un irlandais; constitue son armée; espère rencontrer et y intégrer Eric Sawal; l'assaut commence demain</td>
    <td class="tg-yw3l">Juan; Eric Sawal</td>
    <td class="tg-yw3l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">6791</td>
    <td class="tg-yw4l">2014-11-17 16:51:51</td>
    <td class="tg-yw4l">gilles-froid</td>
    <td class="tg-yw4l">Froid Family</td>
    <td class="tg-yw4l">La douleur</td>
    <td class="tg-yw4l">Gilles Froid après ses aventures dit au revoir à tout le monde et remonte sur Paris</td>
    <td class="tg-yw4l">La Crête; Nicolas; Juan; Isis</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">6795</td>
    <td class="tg-yw4l">2014-11-17 17:47:10</td>
    <td class="tg-yw4l">ingrid</td>
    <td class="tg-yw4l">charlito</td>
    <td class="tg-yw4l">Les potos rapliquent !</td>
    <td class="tg-yw4l">Indique que La Crête devrait bientôt arriver à Paris avec Isis</td>
    <td class="tg-yw4l">La Crête; Isis; Thierry Lun; Cathy Opex</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">6872</td>
    <td class="tg-yw4l">2014-11-17 23:21:53</td>
    <td class="tg-yw4l">ingrid</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Hey hey !!</td>
    <td class="tg-yw4l">La Crête est arrivé au squat, s'apprêtent ce soir à faire la fête et tout casser</td>
    <td class="tg-yw4l">La Crête; Thierry Lun; Cathy Opex; Maurice Upian</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">6986</td>
    <td class="tg-yw3l">2014-11-19 16:04:05</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Ça va chauffer !</td>
    <td class="tg-yw3l">Dit à Ingrid: "On parle de nous!"</td>
    <td class="tg-yw3l">Ingrid</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">7027</td>
    <td class="tg-yw4l">2014-11-19 16:04:24</td>
    <td class="tg-yw4l">ingrid</td>
    <td class="tg-yw4l">charlito</td>
    <td class="tg-yw4l">Casse de jour</td>
    <td class="tg-yw4l">Ingrid parle des plans de La Crête comme de faire un casse de jour pour renverser la police; se pose des questions à ce sujet</td>
    <td class="tg-yw4l">La Crête; Maurice Upian</td>
    <td class="tg-yw4l">La crête leader de mouvement, preneur d'initiatives</td>
  </tr>
  <tr>
    <td class="tg-yw4l">7137</td>
    <td class="tg-yw4l">2014-11-18 13:49:18</td>
    <td class="tg-yw4l">lilou</td>
    <td class="tg-yw4l">The Peacemaker</td>
    <td class="tg-yw4l">Virage</td>
    <td class="tg-yw4l">Lilou lance un grand appel aux éveillés dont La Crête: continuer à se battre avec moins de violence.</td>
    <td class="tg-yw4l">Eric Sawal;  @Victor , @Liza Papanov , @Amandine , @Guy, @EveillésManifeste  , @David Durel , @Cathy Opex, @La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">7230</td>
    <td class="tg-yw4l">2014-11-19 09:54:04</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">19 Nov - Rencontre avec Isis et La Crête</td>
    <td class="tg-yw4l">Eric Sawal rencontre Isis et La Crête et réfléchit sur leur volonté d'organiser une lutte armée et une révolution, au lieu du sitting pacifiste des "bobos" que sont les éveillés</td>
    <td class="tg-yw4l">Isis; La Crête</td>
    <td class="tg-yw4l">Force de proposition, nourrit commentaires et discussions</td>
  </tr>
  <tr>
    <td class="tg-yw3l">7236</td>
    <td class="tg-yw3l">2014-11-20 01:33:02</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Ça va le faire... On y crois</td>
    <td class="tg-yw3l">Les Oubliés vont prendre physiquement le contrôle de Paris: La Crête explique son plan pour bloquer la ville</td>
    <td class="tg-yw3l">Ingrid; Victor; Guy; Jérôme Mariva; Un Petit Rien; Eric Sawal; Liza Papanov; Juan; Ellie; Gilles Froid</td>
    <td class="tg-yw3l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">7555</td>
    <td class="tg-yw4l">2014-11-19 10:16:10</td>
    <td class="tg-yw4l">franceinde</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">la vérité sur le meurtre de Jérémie</td>
    <td class="tg-yw4l">Informe de l'assassinat de l'adolescent Jérémie par les forces de l'ordre et défend le programme pacifiste des Eveillés. S'adresse à ses abonnés dont La Crête</td>
    <td class="tg-yw4l">Marianne Guérin; Charlito; Lilou; Tiphaine; Un Petit Rien; Victor; Alex V.; justin tresor; Walter; Bias de Priène; Gritusse; Matt; Auguste Baboeuf; La Crête; Capucine Cher</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">7559</td>
    <td class="tg-yw4l">2014-11-19 14:38:03</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">19 Nov - Convergences impossibles.</td>
    <td class="tg-yw4l">Eric Sawal manifeste son désaccord envers La Crête et Isis qui s'éloignent des éveillés pour se dédier à la violence sans proposer avantage de solutions pour autant</td>
    <td class="tg-yw4l">Stella; Isis; La Crête</td>
    <td class="tg-yw4l">La Crête est critiqué pour ses positions politiques</td>
  </tr>
  <tr>
    <td class="tg-yw3l">7735</td>
    <td class="tg-yw3l">2014-11-21 14:45:15</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">A l'attaaaaaaaaaaaque !</td>
    <td class="tg-yw3l">Apprend qu'Isis est en prison;critique les Eveillés;Rencontre Eric Sawal qu'il intègre à son armée</td>
    <td class="tg-yw3l">Victor; Charlito; Guy; Lilou; Eric Sawal</td>
    <td class="tg-yw3l">La Crête étend son réseau</td>
  </tr>
  <tr>
    <td class="tg-yw4l">7867</td>
    <td class="tg-yw4l">2014-11-21 13:56:56</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">21 Nov - Isis en taule</td>
    <td class="tg-yw4l">S'adresse à La Crête: vient d'apprendre que les amis de la Crête Ellie, Isis et Juan sont en prison; Eric est prêt à se joindre à lui pour les libérer</td>
    <td class="tg-yw4l">La Crête; Juan; Isis; Ellie</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">7878</td>
    <td class="tg-yw4l">2014-11-21 15:09:33</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">21 Nov - En route avec La Crête</td>
    <td class="tg-yw4l">Rejoint La Crête dans son plan de sauvetage d'Isis, Juan et Ellie; est prêt à mourir</td>
    <td class="tg-yw4l">La Crête; Juan; Isis; Ellie</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">7898</td>
    <td class="tg-yw4l">2014-11-21 18:28:18</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">la constituante</td>
    <td class="tg-yw4l">Affirme au nom des éveillés des réflexions sur le programme politique qui permettra de sortir de la crise et les moyens de l'atteindre. Critique ceux qui posent des bombes comme La Crête.</td>
    <td class="tg-yw4l">Un Petit Rien; Bias de Priène; Gritusse; Tiphaine; Victor; Charlito; Auguste Baboeuf; Stelise; Alex V.; Capucine Cher; Marianne Guérin; Parti européiste; justin tresor; Liza Papanov; Lilou; Tous Ensemble; Walter</td>
    <td class="tg-yw4l">La Crête critiqué pour ses actions violentes</td>
  </tr>
  <tr>
    <td class="tg-yw4l">7900</td>
    <td class="tg-yw4l">2014-11-21 18:50:01</td>
    <td class="tg-yw4l">victor</td>
    <td class="tg-yw4l">climo</td>
    <td class="tg-yw4l">La Crète</td>
    <td class="tg-yw4l">Evoque la même scène que Gally au post précédent: La Crête en punk-à-chien qui demande qui est le taulier ici, Victor répond qu'il n'y a pas de chef</td>
    <td class="tg-yw4l">Guy; La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">7975</td>
    <td class="tg-yw3l">2014-11-22 12:08:05</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">SVP</td>
    <td class="tg-yw3l">S'adresse à Eric Sawal sur la stratégie pour leur assaut du commissariat de Boulogne Billancourt visant à libérer leurs copains</td>
    <td class="tg-yw3l">Eric Sawal</td>
    <td class="tg-yw3l">La Crête force de proposition: soumet directement au personnage d'Eric Sawal un plan pour qu'il le suive avec lui</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8031</td>
    <td class="tg-yw4l">2014-11-22 14:52:28</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">Le plan de La Crête</td>
    <td class="tg-yw4l">Eric se dit d'accord avec le plan de La Crête mais il faut éviter au maximum la violence</td>
    <td class="tg-yw4l">La Crête</td>
    <td class="tg-yw4l">Atteste de l'influence de La Crête au niveau narratif: plan accepté et Eric suit son initiative</td>
  </tr>
  <tr>
    <td class="tg-yw3l">8069</td>
    <td class="tg-yw3l">2014-11-22 19:22:16</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Sur un air de Wagner</td>
    <td class="tg-yw3l">Attaque du commissariat de B. Billancourt;La Crête se prend deux balles;Isis est libérée mais blessée aussi</td>
    <td class="tg-yw3l">Eric Sawal; Juan; Ellie; Amandine</td>
    <td class="tg-yw3l">Narration d'une attaque dont La Crête est le meneur</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8400</td>
    <td class="tg-yw4l">2014-11-23 11:26:15</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">Se coordonner dans la lutte</td>
    <td class="tg-yw4l">Eric reproche un manque de coordination dans la lutte de la part de La Crête et Isis; il leur demande où ils ont placé des bombes et refuse pour sa part d'en faire sauter</td>
    <td class="tg-yw4l">La Crête; Isis</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">8403</td>
    <td class="tg-yw4l">2014-11-23 11:50:50</td>
    <td class="tg-yw4l">victor</td>
    <td class="tg-yw4l">climo</td>
    <td class="tg-yw4l">La Terreur</td>
    <td class="tg-yw4l">Apprend et condamne les attentats de La Crête et autres Oubliés. Discute avec Guy parmi les éveillés dans l'objectif de créer une nouvelle constitution d'ici 15 jours</td>
    <td class="tg-yw4l">La Crête; Guy; Tiphaine; Lilou</td>
    <td class="tg-yw4l">La Crête reconnu dans son pouvoir d'influence: "maintenant c'est lui qui mène la danse"</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8414</td>
    <td class="tg-yw4l">2014-11-23 12:26:11</td>
    <td class="tg-yw4l">erick-foax</td>
    <td class="tg-yw4l">JeremieCED</td>
    <td class="tg-yw4l">Message à la république déchue</td>
    <td class="tg-yw4l">Le leader des Constetacio armés déclare son soutien à plusieurs révolutionnaires dont La Crête</td>
    <td class="tg-yw4l">Eveillés Manifeste; Charlito; La Crête; Anonymous; justin tresor; Jeanne (Juanita)</td>
    <td class="tg-yw4l">Visibilité, notoriété : cité et soutenu</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8417</td>
    <td class="tg-yw4l">2014-11-23 15:32:30</td>
    <td class="tg-yw4l">eric-sawal</td>
    <td class="tg-yw4l">Eric Sawal</td>
    <td class="tg-yw4l">22 Nov - A l'assaut du commissariat de Bobigny</td>
    <td class="tg-yw4l">Eric raconte l'assaut du commissariat de Bobigny où il suivait le plan de La Crête; il finit par tuer un jeune policier et s'en trouve terriblement choqué, même si la cause est juste…</td>
    <td class="tg-yw4l">La Crête; Isis; Ellie; Juan</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">8431</td>
    <td class="tg-yw4l">2014-11-24 01:14:16</td>
    <td class="tg-yw4l">ellie</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Qu le spectacle commence</td>
    <td class="tg-yw4l">Pour entrer au zoo, avec Juan, Isis et La Crête, Ellie entreprend de séduire le vigile qui garde de l'entrée avant de l'assommer</td>
    <td class="tg-yw4l">Juan; La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">8482</td>
    <td class="tg-yw4l">2014-11-24 11:25:04</td>
    <td class="tg-yw4l">erick-foax</td>
    <td class="tg-yw4l">JeremieCED</td>
    <td class="tg-yw4l">ANNONCE (2)</td>
    <td class="tg-yw4l">Annonce un tir de missiles sur le palais bourbon; déclare son soutien à La Crête et autres révolutionnaires</td>
    <td class="tg-yw4l">justin tresor; La Crête; Lilou</td>
    <td class="tg-yw4l">Marque de soutien envers La Crête</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8606</td>
    <td class="tg-yw4l">2014-11-23 19:13:44</td>
    <td class="tg-yw4l">gilles-froid</td>
    <td class="tg-yw4l">Froid Family</td>
    <td class="tg-yw4l">Semaine d'isolement</td>
    <td class="tg-yw4l">Suite à l'enterrement de Clarisse, s'isole une semaine chez ses parents; entend parler des "méfaits" de La Crête et ses amis</td>
    <td class="tg-yw4l">Juan; Isis; James; Nicolas; La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">8611</td>
    <td class="tg-yw4l">2014-11-23 19:51:17</td>
    <td class="tg-yw4l">victor</td>
    <td class="tg-yw4l">climo</td>
    <td class="tg-yw4l">Chère Isis</td>
    <td class="tg-yw4l">Long message à Isis pour exprimer les idées des Eveillés, la constituante, et lui proposer de rejoindre le mouvement. Explique sa vision d'une action non violente et critique les attentats de la Crête, un "meurtrier".</td>
    <td class="tg-yw4l">Isis; Eric Sawal; La Crête; Tiphaine; Un Petit Rien; Guy; EveillésManifeste; Lilou; Marianne Guérin; Charlito</td>
    <td class="tg-yw4l">La Crête critiqué pour ses actions violentes</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8636</td>
    <td class="tg-yw4l">2014-11-23 21:58:58</td>
    <td class="tg-yw4l">amandine</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Est-ce que je suis aussi une hors-la-loi?</td>
    <td class="tg-yw4l">Amandine aide l'infirmière Lilou à soigner les gens; elle soigne un jour quelqu'un dont elle apprend ensuite par la presse que c'est la Crête, terroriste et ennemi n°1. Ne sais plus vers qui se tourner, veut parler à Guy.</td>
    <td class="tg-yw4l">Lilou; Stella; Eric Sawal; La Crête</td>
    <td class="tg-yw4l">La Crête consacré comme leader: son visage fait la une des journaux, il est considéré comme l'un des chefs des Oubliés et ennemi public n°1 suite à ses attentats - au point que le seul fait de l'avoir soigné risque de mettre Amandine en danger</td>
  </tr>
  <tr>
    <td class="tg-yw4l">8651</td>
    <td class="tg-yw4l">2014-11-23 22:56:18</td>
    <td class="tg-yw4l">amandine</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Le complot</td>
    <td class="tg-yw4l">Alors qu'Amandine va voir Guy pour lui parler de son problème avec La Crête, ils discutent finalement de tout autre chose: l'assassinat de Jeremie et son meurtrier</td>
    <td class="tg-yw4l">La Crête; Charlito; Victor; Anonymous; Un Petit Rien; France BN; Sabrina; Lilou</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">8676</td>
    <td class="tg-yw4l">2014-11-24 15:00:16</td>
    <td class="tg-yw4l">laura</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Rejoindre les Eveillés</td>
    <td class="tg-yw4l">Laura rejoint les Eveillés pour les aider, et rencontre un "dingue", La Crête, qui part avec Eric et dit qu'il veut libérer Isis.</td>
    <td class="tg-yw4l">Eric Sawal; Charlito; Victor; Lilou; Stella; La Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">8732</td>
    <td class="tg-yw3l">2014-11-25 03:51:16</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">On fait comme dans la cavalerie et on se tire ailleurs</td>
    <td class="tg-yw3l">Au Sénat, en plein assaut des forces de l'ordre, réflexion sur ses amis et alliés ;Reste en arrière pour retarder les flics ;Se fait attraper et menotter</td>
    <td class="tg-yw3l">@Eric_Sawal ; @Juan ; @Ellie ; @Victor ; @Guy ;</td>
    <td class="tg-yw3l">Égo-centrisme du personnage : «  c'est moi le patron, le grand PDG ! »Prise de pouvoir dans la fiction participe de l'influence de l'auteurÉcrit ce scénario à 4h du matin pour « mener la danse » des contributions des autres</td>
  </tr>
  <tr>
    <td class="tg-yw3l">8753</td>
    <td class="tg-yw3l">2014-11-25 09:59:48</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">REFUSE CE TEXTE</td>
    <td class="tg-yw3l">Détournement du post pour faire une adresse directe à l'auteure (Gally) du personnage de Guy</td>
    <td class="tg-yw3l">@Guy</td>
    <td class="tg-yw3l">L'auteur se positionne en organisateur</td>
  </tr>
  <tr>
    <td class="tg-yw3l">8798</td>
    <td class="tg-yw3l">2014-11-25 13:33:34</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Voir avec @JeanSé</td>
    <td class="tg-yw3l">Etant en prison, LaCrete demande aux personnages de plutôt s'adresser à son second JeanSé ;Annonce la prochaine action de JeanSé (opération kamikaze)</td>
    <td class="tg-yw3l">@Erick Foax ; @Ellie ; @Victor ; @Guy ; @Juan ; @Gilles Froid ; @Anonymous ; @Liza Papanov ; @Eric Sawal</td>
    <td class="tg-yw3l">L'auteur fait un appel à contribution « si vous voulez me suivre, faite le savoir »</td>
  </tr>
  <tr>
    <td class="tg-yw4l">9040</td>
    <td class="tg-yw4l">2014-11-25 02:09:05</td>
    <td class="tg-yw4l">ingrid</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Ça craint pour moi !</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">9060</td>
    <td class="tg-yw4l">2014-11-25 11:46:01</td>
    <td class="tg-yw4l">erick-foax</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Appel à la révolution</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">9102</td>
    <td class="tg-yw4l">2014-11-25 17:53:12</td>
    <td class="tg-yw4l">france-bn</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Message de FRANCE BreakingNews</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">9143</td>
    <td class="tg-yw4l">2014-11-25 22:51:38</td>
    <td class="tg-yw4l">victor</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Pas de répit ?</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">9154</td>
    <td class="tg-yw4l">2014-11-25 23:31:34</td>
    <td class="tg-yw4l">france-bn</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Paris plongé dans l'obscurité</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">9211</td>
    <td class="tg-yw3l">2014-11-26 02:32:23</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Soirée Portes ouvertes</td>
    <td class="tg-yw3l">Séjour en prison, LaCrête ne lâche rien ;Le soir, une panne générale permet aux prisonniers de s'évader ;LaCrête se rend chez Al et JeanSé</td>
    <td class="tg-yw3l">@Al;</td>
    <td class="tg-yw3l">JeanSé est cité, mais pas adressé.. car c'est également son personnage</td>
  </tr>
  <tr>
    <td class="tg-yw3l">9306</td>
    <td class="tg-yw3l">2014-11-26 15:23:43</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Le Procès</td>
    <td class="tg-yw3l">Adresse direct aux auteurs pour « jouer » un personnage de procureur</td>
    <td class="tg-yw3l">@Charles Vennec ; @Liza Papanov ; @Deadlock ; @Erick Foax</td>
    <td class="tg-yw3l">l'auteur se positionne en organisateurLe texte sort de la narration</td>
  </tr>
  <tr>
    <td class="tg-yw4l">9317</td>
    <td class="tg-yw4l">2014-11-26 16:04:51</td>
    <td class="tg-yw4l">maitre-benayoun</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Cette affaire est pour moi.</td>
    <td class="tg-yw4l">L'avocate se déclare prête et déterminée à défendre LaCrête</td>
    <td class="tg-yw4l">@La_Crête</td>
    <td class="tg-yw4l">Déclaration d'un auteur (heyMamzelleBook) pour s'introduire dans le scénario de LaCrête</td>
  </tr>
  <tr>
    <td class="tg-yw4l">9411</td>
    <td class="tg-yw4l">2014-11-26 19:18:11</td>
    <td class="tg-yw4l">al</td>
    <td class="tg-yw4l">The Peacemaker</td>
    <td class="tg-yw4l">Jeux d'enfants</td>
    <td class="tg-yw4l">Al chasse du fachos dans Paris ;Annonce qu'il contactera Erick_Foax et rencontrera Eric_Sawal</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">En certaine contradiction avec le texte « Soirée Portes ouvertes »</td>
  </tr>
  <tr>
    <td class="tg-yw4l">9623</td>
    <td class="tg-yw4l">2014-11-27 12:09:45</td>
    <td class="tg-yw4l">jean-legal</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">quel procès de La Crête?</td>
    <td class="tg-yw4l">Le juge découvre que LaCrête est en prison</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Gally joue le jeu de Ndish en introduisant un personnage juge</td>
  </tr>
  <tr>
    <td class="tg-yw4l">9939</td>
    <td class="tg-yw4l">2014-11-28 11:35:59</td>
    <td class="tg-yw4l">maitre-benayoun</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">A lui de décider</td>
    <td class="tg-yw4l">L'avocat s'étonne qu'elle n'ait pas été choisie</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Insistance de heyMamzelleBook pour interagir avec LaCrête</td>
  </tr>
  <tr>
    <td class="tg-yw3l">9946</td>
    <td class="tg-yw3l">2014-11-28 12:20:59</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Une bavarde pour me défendre</td>
    <td class="tg-yw3l">L'avocat commis d'office annonce à LaCrête qu'il sera représenté par une avocate très compétente</td>
    <td class="tg-yw3l">@Maitre benayoun</td>
    <td class="tg-yw3l">Ndish annonce par là qu'il accepte la proposition de heyMamzelleBook</td>
  </tr>
  <tr>
    <td class="tg-yw4l">10272</td>
    <td class="tg-yw4l">2014-11-28 23:17:09</td>
    <td class="tg-yw4l">mathilde-pevensie</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Première Allocution de la Reine de Bretagne</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">surenchère d'adresse</td>
  </tr>
  <tr>
    <td class="tg-yw4l">10750</td>
    <td class="tg-yw4l">2014-11-30 14:20:56</td>
    <td class="tg-yw4l">jean-legal</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">L'affaire Jérémie</td>
    <td class="tg-yw4l">Le juge découvre qu'il est en charge de l'affaire LaCrête et de l'affaire Jérémie</td>
    <td class="tg-yw4l">@La_Crête</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">11775</td>
    <td class="tg-yw4l">2014-12-01 23:30:47</td>
    <td class="tg-yw4l">maitre-benayoun</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Premier rendez-vous</td>
    <td class="tg-yw4l">L'avocate rencontre LaCrête ;Ell lui explique ce qu'il va se passer : transfert à l'hopital.</td>
    <td class="tg-yw4l">@La_Crête ; @jean-legal</td>
    <td class="tg-yw4l">heyMamzelleBook semble vouloir mener la danse à travers un entretien où son personnage « prend la main ».</td>
  </tr>
  <tr>
    <td class="tg-yw4l">11778</td>
    <td class="tg-yw4l">2014-12-01 23:33:22</td>
    <td class="tg-yw4l">maitre-benayoun</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Petite requête</td>
    <td class="tg-yw4l">L'avocate écrit au juge pour demander le transfert</td>
    <td class="tg-yw4l">@La_Crête ; @jean-legal</td>
    <td class="tg-yw4l">Un post pour confirmer le scénario décrit dans « Premier Rendez-vous »</td>
  </tr>
  <tr>
    <td class="tg-yw4l">11909</td>
    <td class="tg-yw4l">2014-12-02 13:46:15</td>
    <td class="tg-yw4l">jean-legal</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">transfert de M. La Crête</td>
    <td class="tg-yw4l">Le juge répond à l'avocate et confirme le transfert de LaCrête</td>
    <td class="tg-yw4l">@La_Crête ; @Maitre benayoun</td>
    <td class="tg-yw4l">Collaboration à 3, le juge, l'avocate et le prisionnier se sont coordonnés.</td>
  </tr>
  <tr>
    <td class="tg-yw4l">11928</td>
    <td class="tg-yw4l">2014-12-02 14:57:42</td>
    <td class="tg-yw4l">matt</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Lost in translation</td>
    <td class="tg-yw4l">Matt raconte les efforts de son équipe pour hacker les mails du juge et obtenir des infos ;Ils apprennent que LaCrête sera transféré à l'hopital</td>
    <td class="tg-yw4l">@Jean Légal ; @La_Crête ; @Juan</td>
    <td class="tg-yw4l">heyMamzelleBook prépare la suite du scénario</td>
  </tr>
  <tr>
    <td class="tg-yw3l">12325</td>
    <td class="tg-yw3l">2014-12-03 00:08:37</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">domage</td>
    <td class="tg-yw3l">Interaction suite à une tentative d'assassinat de LaCrête en prison</td>
    <td class="tg-yw3l">@Dr Bamoul</td>
    <td class="tg-yw3l">Interaction entre auteurs : Ndish s'en prend à un autre auteur suite à une tentative d'assassinat de son personnage</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12338</td>
    <td class="tg-yw4l">2014-12-03 00:11:42</td>
    <td class="tg-yw4l">dr-bamoul</td>
    <td class="tg-yw4l">Kate</td>
    <td class="tg-yw4l">Encore...</td>
    <td class="tg-yw4l">Evoque une prochaine tentative d'empoisonnement de LaCrête par un compliceEvoque aussi un « appel » au personnel du commissariat</td>
    <td class="tg-yw4l">@La_Crête</td>
    <td class="tg-yw4l">Kate répond à Ndish en restant dans la fiction</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12339</td>
    <td class="tg-yw4l">2014-12-03 00:58:13</td>
    <td class="tg-yw4l">La Crête</td>
    <td class="tg-yw4l">Ndish</td>
    <td class="tg-yw4l">Jouer n'est pas tuer !</td>
    <td class="tg-yw4l">Hors-fiction</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Adresse directe à l'auteur « Kate », Ndish indique que son personnage est « important »Indication du mode de jeu « etre connecté tous les soir » pour éviter les killsÉvoque une ancienne collaboration : regretJustifie son mode de participation</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12363</td>
    <td class="tg-yw4l">2014-12-03 02:18:03</td>
    <td class="tg-yw4l">maitre-benayoun</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Revirement</td>
    <td class="tg-yw4l">l'avocate raconte l'évasion de LaCrête avec l'attaque du fourgon de transfert</td>
    <td class="tg-yw4l">@La_Crête ; @Juan</td>
    <td class="tg-yw4l">D'autres personnages sont présents dans la scène, mais ne sont pas adressés : est ce que leurs auteurs sont moins dans le coup, est ce qu'il est question de distribuer des points à ses alliés ?</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12399</td>
    <td class="tg-yw4l">2014-12-03 09:43:17</td>
    <td class="tg-yw4l">dr-bamoul</td>
    <td class="tg-yw4l">Kate</td>
    <td class="tg-yw4l">la violence appelle la violence</td>
    <td class="tg-yw4l">Hors-fiction</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Interaction entre auteurs : Réponse à NdishKate annonce qu'elle laissera tranquille le personnage de Ndish</td>
  </tr>
  <tr>
    <td class="tg-yw3l">12407</td>
    <td class="tg-yw3l">2014-12-03 10:48:44</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Libre de nouveau</td>
    <td class="tg-yw3l">Evoque son évasion ;Se plaint de sa blessure ;Annonce la suite avec la collaboration avec Eric_Sawal</td>
    <td class="tg-yw3l">@Al, @Eric_Sawal</td>
    <td class="tg-yw3l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">12412</td>
    <td class="tg-yw3l">2014-12-03 11:48:06</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">T'es content !</td>
    <td class="tg-yw3l">Hors-fiction</td>
    <td class="tg-yw3l">@France_BN</td>
    <td class="tg-yw3l">Interaction entre auteurs : Ndish s'en prend à un auteur à cause de sa stratégie de point (profiter de l'écriture et des scénarios de Ndish notamment)</td>
  </tr>
  <tr>
    <td class="tg-yw3l">12468</td>
    <td class="tg-yw3l">2014-12-03 15:46:02</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Communiqué de presse de La Crête depuis l'Élysée</td>
    <td class="tg-yw3l">Depuis l'Elysée ;Manifeste pour l'action des Oubliés : position anarchiste</td>
    <td class="tg-yw3l">@Erick Foax ; @Maitre benayoun ; @Dr Bamoul ; @Charles Vennec ; @Victor  ; @EveillésManifeste ; @France BN ; @Tous Ensemble ; @Colonel Atlas ; @Devi Sweetie  ; @Jerôme Mariva ; @Gilles Froid ; @Eric Sawal; @Charlito</td>
    <td class="tg-yw3l">l'auteur justifie l'action de son personnage</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12485</td>
    <td class="tg-yw4l">2014-12-03 15:43:04</td>
    <td class="tg-yw4l">lilou</td>
    <td class="tg-yw4l">The Peacemaker</td>
    <td class="tg-yw4l">Disparition des frontières</td>
    <td class="tg-yw4l">Lilou soigne LaCrête</td>
    <td class="tg-yw4l">@La_Crête ; @Ellie</td>
    <td class="tg-yw4l">Un auteur complice (The Peacemaker) raconte les coulisses du personnage LaCrête</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12671</td>
    <td class="tg-yw4l">2014-12-03 08:56:33</td>
    <td class="tg-yw4l">jean-legal</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">What the Fuck?</td>
    <td class="tg-yw4l">Le juge prend acte de l'évasion de LaCrête</td>
    <td class="tg-yw4l">@La Crête ; @France BN ; @Maurice Upian ; @justin tresor ; @Camélia Narchie</td>
    <td class="tg-yw4l">Clôture narrative de l'arche « prison et procès de LaCrête »</td>
  </tr>
  <tr>
    <td class="tg-yw4l">12823</td>
    <td class="tg-yw4l">2014-12-04 01:13:37</td>
    <td class="tg-yw4l">ellie</td>
    <td class="tg-yw4l">heyMamzelleBook</td>
    <td class="tg-yw4l">Qu'ils aillent au diable !</td>
    <td class="tg-yw4l">La bande LaCrête, Juan, Ellie, Isis veulent quitter l'Elysée avec le président Larcher en otage.Ils sont excédés par les discussions politiques des Eveillés</td>
    <td class="tg-yw4l">@Juan ; @LaCrête ;</td>
    <td class="tg-yw4l">On sent la collaboration entre The Peacemaker, heyMamzelleBook et Ndish</td>
  </tr>
  <tr>
    <td class="tg-yw4l">13148</td>
    <td class="tg-yw4l">2014-12-04 04:27:49</td>
    <td class="tg-yw4l">juan</td>
    <td class="tg-yw4l">The Peacemaker</td>
    <td class="tg-yw4l">LARCHER LES AMARRES!</td>
    <td class="tg-yw4l">Juan raconte l'évasion de l'Elysée ;Braquage d'une voiture et ordonne au conducteur de raconter que LaCrête a enlevé le Président Larcher ;Affolement ;Retour au van abandonné au Jardin du Luxembourg pour récupérer du matériel ;Arrivés à la péniche</td>
    <td class="tg-yw4l">@Isis ; @LaCrête ; @Ellie</td>
    <td class="tg-yw4l">« Il éclata de rire en me frottant la tête de façon paternel. » l'auteur est sous le charme du paternalisme du personnage de LaCrête (et en fait de son auteur Ndish)Vrai effort littéraire, sans doute pour obliger la rédaction à suivre le scénario des trois complices (à vérifier)</td>
  </tr>
  <tr>
    <td class="tg-yw4l">13177</td>
    <td class="tg-yw4l">2014-12-04 12:42:36</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">aller, c'est parti pour les débats et les votes!</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">13221</td>
    <td class="tg-yw4l">2014-12-04 16:02:04</td>
    <td class="tg-yw4l">occitania-info</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Premiers pas d'info !</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Nouveau personnage, intègre LaCrête et d'autres personnage centraux dans sa communication pour augmenter sa visibilité</td>
  </tr>
  <tr>
    <td class="tg-yw4l">13249</td>
    <td class="tg-yw4l">2014-12-04 21:44:51</td>
    <td class="tg-yw4l">victor</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">Gagner du temps</td>
    <td class="tg-yw4l">Victor résume la situation ;Il rejoint le Colonel Atlas</td>
    <td class="tg-yw4l">@Guy ; @Colonel Atlas ; @Juan ; @Isis ; @La Crête ; @Charlito</td>
    <td class="tg-yw4l">L'auteur Climo prend acte des actions de la journée, en faisant un résumé de la situationA travers son personnage impatient, Climo semble envoyer un message codé aux autres auteurs pour qu'ils réagissent à la situation et avancent dans la narration. Il laisse la main aux personnages importants (Charlito, Guy)</td>
  </tr>
  <tr>
    <td class="tg-yw3l">13858</td>
    <td class="tg-yw3l">2014-12-06 02:10:51</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Vivre à L’air libre</td>
    <td class="tg-yw3l">LaCrête raconte ce moment de liberté et l'union du petit groupe ;Annonce un plan à venir avec 3 attaques successives</td>
    <td class="tg-yw3l">@Juan ; @Ellie</td>
    <td class="tg-yw3l">« On avait largué Larcher, ce type ne sert vraiment à rien, » potentiellement un signal à la Rédaction pour dire de laisser tomber le personnage</td>
  </tr>
  <tr>
    <td class="tg-yw4l">14302</td>
    <td class="tg-yw4l">2014-12-06 20:54:51</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Valois à Paris dans 72H?</td>
    <td class="tg-yw4l">Guy reprend l'Actualité de la rédaction et semble faire un appel à résister à Valois</td>
    <td class="tg-yw4l">@Gritusse ; @Victor ; @Charlito ; @La Crête ; @Capucine Cher ; @Marie Dujardin  ; @TANGUY CRS 4587 ; @justin tresor ; @Eric Sawal ; @Erick Foax  ; @France BN ; @Christophe Flavier ; @Walter  ; @Stella</td>
    <td class="tg-yw4l">Ici le personnage s'inscrit dans l'actualité rédigée par la Rédaction : à savoir la marche de l'armée de Valois sur Paris</td>
  </tr>
  <tr>
    <td class="tg-yw4l">14896</td>
    <td class="tg-yw4l">2014-12-07 17:49:15</td>
    <td class="tg-yw4l">adrien</td>
    <td class="tg-yw4l">SweeeN</td>
    <td class="tg-yw4l">Deux bras, deux jambes</td>
    <td class="tg-yw4l">adrien demande aux personnages influents s'il peut les rejoindre</td>
    <td class="tg-yw4l">@EveillésManifeste ; @La Crête; @Isis  ; @Al ; @Guy</td>
    <td class="tg-yw4l">L'auteur fait ici un appel aux auteurs qui comptentSemble ne pas avoir lu les contributions des auteurs, il est hors-fiction</td>
  </tr>
  <tr>
    <td class="tg-yw4l">14909</td>
    <td class="tg-yw4l">2014-12-07 18:24:21</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">bienvenue</td>
    <td class="tg-yw4l">réponse à adrienRésume la situation, notamment vis-à-vis des Oubliés</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">l'auteur accepte l'aide proposée et indique même au nouveau venu une action de résistance à mener</td>
  </tr>
  <tr>
    <td class="tg-yw4l">15571</td>
    <td class="tg-yw4l">2014-12-08 10:00:56</td>
    <td class="tg-yw4l">laura</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">on est prêt</td>
    <td class="tg-yw4l">Raconte le plan de résistance à Vallois</td>
    <td class="tg-yw4l">@Victor ; @Eric Sawal ; @Erick Foax ; @Adrien ; @Charlito ; @Xavier ; @Lilou ; @Stella ; @Général Atlas ; @général crelcel ; @Hervé Le Bras ; @Laurent Lerouge ; @George Decointe ; @La Crête</td>
    <td class="tg-yw4l">l'auteur organise la suite, suggère un scénario à la Rédaction (qui mène Vallois), laisse entendre que si ca ne se passe pas comme elle a prévu, elle est prête à un scénario alternatifElle met dans le coup tous les personnages importants, notamment les généraux qui font l'Actu, ainsi que les Oubliés qui restent influents et qui peuvent changer la donne narrative« @LaCrete et ses amis » : comme si LaCrête restait le personnage central</td>
  </tr>
  <tr>
    <td class="tg-yw4l">15620</td>
    <td class="tg-yw4l">2014-12-08 14:29:34</td>
    <td class="tg-yw4l">eveillesmanifeste</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">La France restera libre!</td>
    <td class="tg-yw4l">annonce du discours de Guy</td>
    <td class="tg-yw4l">@Fantasio ; @Jacques ; @Gritusse ; @Stelise ; @Kayla aka Midas ; @Matt ; @Bias de Priène ; @Victor ; @Charles Vennec ; @Charlito ; @La Crête ; @Isis ; @Capucine Cher ; @Treizh ; @TANGUY CRS 4587 ; @justin tresor ; @Eric Sawal ; @Erick Foax ; @General Alcatraz  ; @France BN ; @Lilou ; @Aziz ; @Hervé Le Bras ; @Jesus routier ; @La source ; @Walter ; @Adrien</td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l">16804</td>
    <td class="tg-yw4l">2014-12-10 22:52:35</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Discours de Guy -Leader des Eveillés</td>
    <td class="tg-yw4l">Discours</td>
    <td class="tg-yw4l">@Général Atlas ; @général crelcel ; @Colonel du Peyroux ; @Fantasio ; @Jacques ; @Monique ; @Bias de Priène ; @Gritusse ; @Stelise ; + plusieurs dizaines<br></td>
    <td class="tg-yw4l">Appel à toutes les personnages</td>
  </tr>
  <tr>
    <td class="tg-yw4l">16862</td>
    <td class="tg-yw4l">2014-12-10 22:52:35</td>
    <td class="tg-yw4l">guy</td>
    <td class="tg-yw4l">Gally</td>
    <td class="tg-yw4l">Discours de Guy -Leader des Eveillés</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw3l">16886</td>
    <td class="tg-yw3l">2014-12-11 05:30:31</td>
    <td class="tg-yw3l">La Crête</td>
    <td class="tg-yw3l">Ndish</td>
    <td class="tg-yw3l">Lundi 8 décembre 2014</td>
    <td class="tg-yw3l">Attaque de la Banque de France tourne mal ;LaCrête couvre la sortie des autres ;Il se fait abattre par les forces de l'ordre</td>
    <td class="tg-yw3l">@Ellie ; @Juan ; @Erick Foax ; @Guy; @Gilles Froid ; @Eric Sawal ; @Jesus routier</td>
    <td class="tg-yw3l"></td>
  </tr>
</table>

&nbsp;

## Typologie des mentions

On peut identifier ainsi différents usages de la mention, présentés dans cette rapide typologie :

  * une adresse directe de personnage à personnage
  * l'inclusion d'un personnage dans son propre scénario
  * l'utilisation de la mention pour valoriser son posts, y agréger les personnages qui comptent.

Cette typologie suggère **des stratégies** de mention différentes pour des objectifs différents, par exemple :

  * des objectifs de narration : cohérence narrative entre personnages
  * des objectifs de jeu : gagner des points
  * des objectifs d'organisation de la collaboration : se mettre d'accord

Ces premiers éléments d'analyse sur les stratégies d'utilisation de la mention sont les premiers indices d'une autorité narrative que nous développerons dans un post à venir.  
