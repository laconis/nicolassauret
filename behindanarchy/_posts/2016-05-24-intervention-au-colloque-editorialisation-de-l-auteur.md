---
layout: "post"
title: "Colloque Editorialisation de l'auteur"
date: "2016-05-24 11:08"
---

Nous intervenons au colloque [Ecrivains, personnages, profils : l'éditorialisation de l'auteur](http://colloque2016.ecrituresnumeriques.ca/), qui se tient le 24 et 25 mai à l'Université de Montréal.

<a class="btn btn-default btn-sm" href="http://colloque2016.ecrituresnumeriques.ca/infos/" title="Programme colloque Editorialisation de l'auteur">
<i class="fa fa-arrow-right"></i> Programme et informations
</a>

**Argumentaire**

> LES OUTILS DE PRODUCTION DE L’AUTEUR à l’ère numérique
>
> Depuis les années 1990, les écrivains ont progressivement envahi la toile, investissant les blogues et les réseaux sociaux, expérimentant des formes hypermédiatiques inédites. Des communautés en ligne se sont formées, de même que des coopératives d’écrivains, opérant une reconfiguration évidente des rapports entre les instances auctoriale, lectrice et éditoriale. Ces nouvelles pratiques ont un impact certain, mais encore mal défini, sur l’ensemble de l’institution littéraire et en particulier sur le modèle économique éditorial traditionnel. Face à ces mutations, que l’on peut désigner comme des formes d’éditorialisation, la tentation est grande de constater un affaiblissement de la figure auctoriale au profit de la multiplication des œuvres collectives, lesquelles ont par ailleurs remis en question le rôle institutionnel des maisons d’édition. Il est cependant possible d’observer, simultanément, l’émergence de pratiques d’écriture en ligne inédites où la figure auctoriale se met en scène, jouant des tensions entre l'auteur, l’écrivain, le personnage d’écrivain et la personne elle-même. À partir de ce paradoxe, le colloque « Écrivains, personnages, profils : l’éditorialisation de l’auteur » propose d’étudier le statut de l’auteur à l’ère du numérique afin de mesurer l’impact effectif des nouvelles technologies sur le concept d’auctorialité.

## Notre intervention
Nous intervenons dans la seconde session *L'autopublication et ses nouvelles formes de validation*, en interrogeant cette fois-çi la figure de l'auteur dans Anarchy, et sa dispersion dans les figures du personnage, du lecteur et de l'éditeur.

<iframe src="http://www.iri.centrepompidou.fr/dev/~sauretn/anarchy/colloque_edito_auteur/#/"  style="width:100%;height:30vw;border:none;"></iframe>
