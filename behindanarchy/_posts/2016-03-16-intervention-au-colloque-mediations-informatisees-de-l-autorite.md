---
layout: post
title: "Colloque Médiations informatisées de l'autorité"
date: '2016-03-16 18:24'
---

Le 17 et 18 mars 2016 se tient le colloque _«&nbsp;Médiations informatisées de l’autorité : nouvelles écritures, nouvelles pratiques de la reconnaissance&nbsp;?&nbsp;»_.  

<a class="btn btn-default btn-sm" href="http://www.iscc.cnrs.fr/IMG/pdf/20160317-mediations.pdf?1383/224efeba8d92e0b9b58d0d334ae6439a3204c61d" title="Programme colloque MIAu">
<i class="fa fa-arrow-right"></i> Consulter le programme (pdf)
</a>

**Argumentaire**

<img src="{{ site.baseurl }}/assets/img/miau.png" class="img-thumbnail" title="Colloque MIAu" style="float:right;margin-left:20px">

>L’intrusion des technologies numériques dans tous les domaines de l’activité humaine et les transformations des rapports de l’homme avec ses univers professionnel, culturel et social semblent ouvrir une nouvelle ère. L’émergence de nouveaux dispositifs d’une part transforme les formes d’écriture précédentes, et d’autre part textualise des pratiques de communication et d’échange qui jusqu’alors restaient extra-textuelles. Dans ce nouveau contexte médiatique, les processus de la diffusion, de la reconnaissance, de la légitimation, de l’autorisation à la parole, à l’expression se transforment. Les réseaux numériques (lieux d’auto-publication, plate-formes collaboratives, archives ouvertes et sites Web de «&nbsp;réseautage social&nbsp;»…) forment des appareils définissant les conditions tant de la diffusion de travaux et d’œuvres que de la mise en valeur de soi. Des techniques d’audience, relatives au numérique, comme le «&nbsp;buzz&nbsp;» ou les connexions des «&nbsp;profils&nbsp;» et CV entre professionnels, chercheurs, amateurs ou artistes suscitent une remise en question des pratiques concernant la réputation et la visibilité professionnelle ou sociale.
>
>On propose de travailler, dans le cadre de ce colloque, sur les médiations de l’autorité dans le cadre des mutations médiatiques engagées par le numérique : comment la «&nbsp;présence&nbsp;» dans l’espace médiatique numérique impacte-t-elle l’autorité&nbsp;? Quelles définitions faut-il retenir aujourd’hui de ce terme, sachant la complexité de son histoire et la multiplicité de ses formes&nbsp;? Quelles différences et quelles complémentarités penser avec des notions connexes aujourd’hui comme «&nbsp;popularité&nbsp;», «&nbsp;visibilité&nbsp;», «&nbsp;réputation&nbsp;», etc.&nbsp;? Assiste-t-on à l’émergence de nouvelles normes de la légitimation&nbsp;? Quelles articulations s’établissent-elles entre les pratiques d’auto-publication, d’auto-édition, d’autopromotion des textes et la valeur d’autorité&nbsp;? Quel est le point de vue des participants dans les espaces numériques voués à la publication d’échanges&nbsp;? Les espaces numériques peuvent-ils contribuer à la formation de nouvelles «&nbsp;autorités&nbsp;»&nbsp;?
{: style="font-size:0.9em"}

<small>Ce colloque est organisé par le Gripic (Celsa, Paris-Sorbonne), l’Institut des sciences de la communication et le laboratoire Communication et solidarité (université Blaise Pascal Clermont-Ferrand), avec le soutien de l’École doctorale V Concepts et langages de l’université Paris-Sorbonne et de la ville de Clermont-Ferrand.</small>

## Notre intervention

Nous intervenons au colloque dans la troisième session intitulée _Transformation et continuité des «&nbsp;métiers d’écriture&nbsp;» (ou : comment se recompose l’écriture)_ pour présenter les premiers résultats de notre travail de recherche sur le corpus Anarchy.

<iframe src="http://www.iri.centrepompidou.fr/dev/~sauretn/anarchy/colloque_autorite/"  style="width:100%;height:30vw;border:none;"></iframe>

<a class="btn btn-default btn-sm" href="http://www.iri.centrepompidou.fr/dev/~sauretn/anarchy/colloque_autorite/" title="Intervention colloque MIAu">
<i class="fa fa-eye"></i> Ouvrir les slides
</a>
