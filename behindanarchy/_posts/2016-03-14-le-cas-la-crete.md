---
layout: post
title: Le cas La Crête
date: '2016-03-14'
author: Ariane
---

La Crête a été un personnage emblématique de l'univers Anarchy. C'est assez naturellement sur ce personnage et sur son auteur que nous avons concentré notre étude qualitative.

## Portrait robot

<img class="img-thumbnail" src="{{ site.baseurl }}/assets/img/profil_Lacrete.png" width="50%" style="margin-left:10px; float:right;"> Qui est La Crête&nbsp;? Un «&nbsp;punk dans l’âme&nbsp;», nous indique son auteur NDish, sorti en 7ème position du jeu _Anarchy_. Ce jeune homme de 37 ans, sans domicile fixe et sans travail, «&nbsp;écoute du rock alternatif français en boucle, il fréquente les clochards avec qui il boit des bières à longueur de temps&nbsp;», c’est «&nbsp;quelqu’un de simple qui aime lire, il vit de la manche et de petits coups de main qu’il donne de gauche à droite en échange d’une douche, d’une couverture&nbsp;». C’est un débrouillard. Lors des deux mois qu’a duré _Anarchy_, il est sorti de son anonymat pour devenir le leader des Oubliés, avec son acolyte féminine Isis, sœur spirituelle puisqu’elle a aussi pour auteur NDish. Or les Oubliés se sont vite fait connaître. Groupuscule révolutionnaire opposé aux intentions plus pacifistes des Eveillés face à une France sortie de l’euro qui plonge dans le chaos, ils se sont signalés par des actions coup de poing, souvent menées par un La Crête qui multiplie les tentatives d’attentats jusqu’à finir par en perdre précocement la vie, que certains vont même qualifier de terroristes. De notre côté, on a voulu y voir un peu plus clair sur cette figure étrange. Comment peut-on être un anarchiste, qui se moque éperdument des questions d’autorité, d’argent et de pouvoir, tout en finissant par mener le jeu par le bout du nez au point que certains ont dit : «&nbsp;c’est lui qui mène la danse&nbsp;» ? Qui est-il vraiment au final, entre ombre et lumière&nbsp;? Et surtout, comment a-t-il réussi, innocemment ou par stratégie, à devenir un des piliers cruciaux de l’histoire d’_Anarchy_&nbsp;?



## Analyse qualitative

L’analyse des processus de constitution de l’influence narrative chez La Crête – et à travers lui bien sûr, chez son auteur NDish – a fait l’objet d’un volet particulier de nos recherches : l’analyse narrative et qualitative, qui se conjuguait à une analyse quantitative (autorité topologique) et une analyse dite organisationnelle (autorité dispositive). Dans celle-ci, nous avons tenté de mettre en évidence les dynamiques par lesquelles le personnage La Crête a progressivement accru sa visibilité, sa notoriété et sa légitimité dans le dispositif (pour plus de précisions sur ces notions, se reporter au billet «&nbsp;Cadre conceptuel&nbsp;: l’autorité dans un environnement littéraire&nbsp;»). Il s’agissait donc de mesurer à la fois son influence narrative 1) sur la communauté de ses **pairs** et 2) sur le cadre institutionnel représenté par la **Rédaction**. Seulement&nbsp;: comment faire&nbsp;? De quelles ressources disposons-nous pour mesurer ces caractéristiques apparemment abstraites&nbsp;?

#### Les ressources

A partir de l’ensemble des billets de La Crête présents sur le site Anarchy.fr, ainsi que du récit-cadre alimenté par la rédaction, le fil d’actualité, nous avons produit deux outils qui forment le support de nos investigations&nbsp;:

-	A) La chronologie reconstituée des actions de La crête, qui prend la forme d’un tableau où par date sont recensés les résumés de ses billets et de ses interactions&nbsp;;
-	B) Un tableau comparatif qui met les actions du personnage en regard du fil d’actualité de la Rédaction, permettant de donner une visibilité aux actions narratives La Crête qui ont été (ou non) relayées par cette dernière&nbsp;;
-	C) A ces deux outils, s’est ajouté le livre de Marion Guérard (journaliste de la Rédaction) _Anarchy_, aux éditions Les Petits Matins (2015), qui témoigne des rapports de force entre la Rédaction et les joueurs tout au long de l’expérience.

<p><img src="{{ site.baseurl }}/assets/img/anarchy_guenard.jpg" class="img-thumbnail"></p>{: style="text-align:center;"}

Figure : couverture du livre de Marion Guénard : _Anarchy. Ils ont écrit la France du chaos_
{: class="figurelegende"}

#### Les stratégies

Quels résultats avons-nous trouvés&nbsp;? D’abord, la lecture de l’ensemble des billets de La Crête (outil A) permet de dégager trois **stratégies** de construction d’une influence sur ses pairs.

-	La première est le travail littéraire que fait NDish sur le caractère de son personnage, qu’il dote d’une autorité charismatique (cf. le «&nbsp;Testament de mon sentiment moral&nbsp;», 13/11, où il expose ses idées politiques et philosophiques, qui pose ainsi les contours psychologiques et moraux du personnage), et d’une force d’initiative (cf. le plan qu’il soumet à Eric Sawal pour attaquer un commissariat, 22/11 : La Crête entraîne ses camarades à le suivre, ce qui dans ce cas en l’occurrence s’est révélé efficace). Cela lui a permis de se donner dès les premières du jeu une **visibilité** d’où résulte son **influence narrative**.
-	La seconde stratégie est le travail de NDish sur l’impact citationnel de son personnage, à travers sa capacité à relayer de l’information, mais aussi à demander aux autres de relayer ses propres actions (cf. le post du 12/11, où il s’adresse à ses associés pour qu’ils diffusent la nouvelle de son attentat du 5 novembre, s’étonnant que celui-ci soit passé sous silence ; ce que l’organe de presse FranceBN ne tardera pas à faire effectivement). En se constituant en tant que nœud informationnel, il établit sa **notoriété** qui lui confère peu à peu une **influence médiatique**, analogue aux «&nbsp;retweet influence&nbsp;» et «&nbsp;mention influence&nbsp;» qui constituent des métriques de la notoriété des utilisateurs de Twitter (cf Cha _et al._, 2013).
-	Enfin, sa troisième stratégie est un travail sur la représentation de l’auteur lui-même, à travers une justification de son positionnement (cf. son altercation «&nbsp;hors-fiction&nbsp;» avec l’auteur Kate, où il parle en son propre nom – celui de NDish – pour expliquer sa démarche auctoriale), et la constitution d’un réseau de collaborations (particulièrement fructueuse par exemple avec le grand gagnant PeaceMaker, auteur de Juan, cf. post du 04/12). Ce travail sur la figure auctoriale qui intervient directement au sein de son récit (métalepses) lui permet d’instaurer sa **légitimité** sur laquelle il peut bâtir une **influence organisationnelle**.

<p><img src="{{ site.baseurl }}/assets/img/jouernestpastuer1.png" class="img-thumbnail"></p>{: style="text-align:center;"}

Figure : Altercation hors-fiction avec Dr Bamoul, alias Kate
{: class="figurelegende"}

#### Succès rencontré

Mais quel est alors le **succès** rencontré par ces stratégies ? On peut mesurer celui-ci à l’aide de son effet sur deux types d’acteurs, mentionnés plus haut : ses pairs, et la Rédaction.

-	Vis-à-vis de ses pairs, les autres **joueurs**, la réussite des stratégies d’influence déployées par l’auteur de La Crête est attestée par plusieurs indices. C’est 1) d’abord le suivi effectif de ses initiatives narratives (Eric Sawal le 22/11 accepte le plan d’attaque que lui soumet La Crête), puis 2) son aptitude à faire effectivement relayer les informations qu’il diffuse (France BN notamment qui diffuse son attentat le 12/11 en s’adressant à une liste de très nombreux personnages qui pourront lire le message), ainsi que 3) les soutiens, discussions et critiques à son sujet alimentés par d’autres personnages qui parlent de lui entre eux (ce qu’on voit tout particulièrement entre le 16/11 et le 21/11), et enfin 4) la reconnaissance directe de son statut de leader narratif («&nbsp;maintenant, c’est lui qui mène la danse&nbsp;», écrit Victor le 23/11).
-	Qu’en est-il du côté de la **Rédaction**&nbsp;? On remarque que cette dernière ne relaie que plutôt tardivement les actions de La Crête dans le fil d’actualité, mais que celui-ci est toutefois l’un des premiers personnages à en influencer directement le cours. Deux grands événements qui le concernent sont ainsi intégrés à l’arche générale d’_Anarchy_&nbsp;: 1) le récit des méfaits des Oubliés, ennemis publics n°1, et en particulier l’arrestation de La Crête (du 23 au 25/11), et 2) l’annonce du procès de La Crête et de son évasion (02 et 03/11). Mais, outre cet impact sur l’arche narrative, il faut aussi prendre en compte son influence sur le dispositif lui-même. Aux alentours du 20 novembre 2014, relate Marion Guénard de la Rédaction, se produit en effet ce qu’elle appelle le «&nbsp;grand virage&nbsp;» : on «&nbsp;lâche la bride&nbsp;» et laisse carte blanche au récit des joueurs, ce qui se traduit par un réajustement des rythmes de production (les marges d’anticipation sont revues à la baisse, afin de suivre aux mieux les initiatives des personnages). Ce virage a été notamment induit par un rapport de force entre NDish et la Rédaction. Celle-ci a d’abord dû brimer cet «&nbsp;auteur caractériel&nbsp;» qui «&nbsp;s’échine à écrire des histoires qui passent à l’as parce qu’elles ne sont pas sur le tempo de nos flashs télévisés préenregistrés&nbsp;», avant de reconnaître qu’elle risquait de frustrer et de perdre cet élément de talent. C’est suite à ce bras de fer avec NDish que la Rédaction a cédé une partie de son autorité pour offrir plus de place aux joueurs dans le pilotage du récit.

<p><img src="{{ site.baseurl }}/assets/img/victor1.png" class="img-thumbnail"></p>{: style="text-align:center;"}

Figure : Reconnaissance de La Crête par Victor, alias Climo
{: class="figurelegende"}

## Résultats et conclusions

Que peut-on en conclure&nbsp;? Les stratégies de NDish ont donc été efficientes à un double niveau. Celui des autres joueurs, à travers l’attestation d’une influence narrative, médiatique et organisationnelle, mais aussi à celui de la Rédaction, par l’intégration de ses actions dans le récit-cadre ainsi que par le réaménagement du dispositif lui-même. Mais que pouvons-nous en déduire, du point de vue des modes de constitution de l’autorité et de l’influence narrative sur un dispositif collaboratif spécifique à l’espace numérique&nbsp;? Sont-ils analogues à ceux de l’imprimé ou les reconfigurent-ils&nbsp;?

#### Entre autorité et autoritativité

Il semble qu’on observe, dans le cas de ce personnage, une construction de l’autorité qui obéit à une double dynamique ambivalente.

D’un côté, en effet, les stratégies grâce auxquelles La Crête augmente progressivement sa visibilité, sa notoriété et sa légitimité semblent se situe à l’écart et en amont des médiations éditoriales traditionnelles, qui passent par le respect de critères de publication fixés a priori par une institution. De ce point de vue, elles s’inscriraient plutôt dans ce qu’on pourrait appeler avec Evelyne Broudoux une pratique **autoritative**&nbsp;:

«&nbsp;Nous avons qualifié de pratique autoritative […] la propension pour les auteurs à s’affirmer auteurs en dehors des autorités établies, ce qui nous a permis de distinguer l’auteur traditionnel, s’inscrivant dans un dispositif éditorial classique pratiquant un filtrage de la chose publiée en aval de sa production, de l’auteur autoritatif s’autopubliant et construisant lui-même les conditions de sa reconnaissance dans l’univers électronique&nbsp;». (Broudoux _et al._, 2005).

C’est ce dont témoigne le bras de fer entre NDish et la Rédaction, qui est allée jusqu’à réorganiser les règles du jeu pour les rendre plus adéquates aux modes d’écriture de personnages qui, comme La Crête, associent une forte influence narrative auprès des autres joueurs à un faible relayage au niveau du fil d’actualité et du récit officiel. Héritant des pratiques autoritatives émergentes dans l’environnement numérique, dans le contexte d’une création collaborative innovante dont la nature expérimentale laisse aux règles une certaine souplesse, NDish ne fait pas que construire son autorité, il en construit également les critères et les métriques.

D’un autre côté cependant, les méthodes auxquelles recourt NDish pour accroître son influence narrative et médiatique font ressurgir des formes bien plus traditionnelles de formation de l’autorité. Comme on l’a vu, l’auteur qui se trouve derrière le personnage La Crête, à travers son avatar NDish, s’expose à plusieurs reprises dans ses billets lors de métadicours qui visent à justifier sa démarche et son positionnement. Ces moments «&nbsp;hors-fiction&nbsp;», combinés au prises de contact et sollicitations directes de la Rédaction par NDish, laissent penser que celui-ci utilise une forme de charisme personnel pour parvenir à ses fins. La personne biographique de l’auteur, loin de s’effacer dans l’ombre d’un personnage ou de céder le pas à une textualité pure et anonyme, revient au contraire s’affirmer au sein même de l’écriture pour en orienter les rapports de force.

#### L'ordre sans le pouvoir&nbsp;?

Il semble ainsi que l’influence de NDish repose autant sur une pratique autoritative spécifique à un dispositif évolutif et contributif que sur une autorité charismatique (Weber), qui aboutit peu à peu à la consécration légitime d’une autorité institutionnelle. Une fiction numérique collaborative comme _Anarchy_ est donc loin de vérifier l’hypothèse d’une disparition de l’auteur ou d’un retrait numérique de l’autorité qui lui est rattachée, qui feraient le propre de l’univers décentralisé et horizontalisé du web. Des relations de pouvoir aboutissant à l’exercice d’influences sont bien visibles dans ce projet, même si elles apparaissent sous des traits renouvelés par rapport aux dynamiques régissant la publication imprimée&nbsp;: le fait même que La Crête ait à construire les expressions et critères de son autorité, à s’«&nbsp;autoriser&nbsp;» lui-même, en confirme précisément la pertinence.

Les différentes ressources qui ont permis d’établir ces analyses (chronologie des actions de La Crête et tableau comparatif avec le fil d’actualité) sont disponibles sur ce site.

Même si _Anarchy_ est _Anarchy_, et si La Crête est le premier des anarchistes, les mécanismes qui en régissent le fonctionnement ne sont pas si anarchiques qu’ils en ont l’air.
