---
layout: page
title: Recherches
---


{[activités en cours]({{ page.url }}#activités-en-cours)} {[activités passées]({{ page.url }}#activités-passées)} {[publications]({{ page.url }}#publications)}  {[conférences scientifiques]({{ page.url }}#conférences-scientifiques)} {[conférences et ateliers]({{ page.url }}#conférences-et-ateliers)} 
{: .center}


## Activités en cours

**Maître de conférences** en sciences de l'information et de la communication à l'Université Paris 8 Vincennes - Saint-Denis.

**Membre du Comité éditorial** des [Collections ArTeC](https://eur-artec.fr/edition/editorial/).

<i class="fa fa-arrow-right"></i>  **En tant que co-fondateur et éditeur** des [Ateliers de [sens public]](https://ateliers.sens-public.org), je poursuis la réflexion et l'expérimentation éditoriale pour de nouvelles fabriques du savoir.

**Bricoleur** de [pinkmypad.net](https://pinkmypad.net).

## Activités passées

<i class="fa fa-arrow-right"></i>  **En tant que post-doctorant** à l'Université Paris Nanterre (2020-2022), j'ai partagé mon temps entre&nbsp;

- le [FabPart Lab](https://fplab.parisnanterre.fr), projet transversal au [labex _Les passés dans le présent_](https://passes-present.eu). Le FP Lab accompagnait les chercheur·e·s et les étudiant·e·s pour favoriser les approches de médiation, de participation et de valorisation dans leurs projets&nbsp;;
- le [HN Lab](https://www.huma-num.fr/hnlab/) de la TGIR Huma-Num, qui explore les approches, les outils et les services émergents dans la communauté SHS ([carnet de recherche](https://hnlab.huma-num.fr/blog/)).

<i class="fa fa-arrow-right"></i> J'ai co-dirigé avec Marta Severo l'ouvrage collectif « Communautés et pratiques d'écritures des patrimoines et des mémoires », à paraître aux Presses universitaires de Paris Nanterre dans la collection _Intelligences numériques_.

**En tant que doctorant** à l'Université Paris Nanterre et à l'Université de Montréal (2015-2020) : 

<i class="fa fa-arrow-right"></i> **Coordinateur scientifique du projet Revue 2.0** à la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca).

- le projet [_Revue 2.0_](http://revue20.org/) vise à repenser le rôle des revues savantes dans les sciences humaines et sociales et à accompagner les acteurs de l’édition scientifique dans leur transition numérique.
- subvention Développement partenariat du CRSH/SSHRC pour 3 an (2018-2021).
- 6 revues partenaires + Érudit, OpenEdition, Human-Num.

<i class="fa fa-arrow-right"></i> **Dossier _Écrire les communs, au devant de l'irréversible_**

- dossier en édition continue pour la revue _Sens public_, initié avec [Sylvia Fredriksson](http://www.sylviafredriksson.net/) en mai 2018.
- rendu public le 1er mars 2019, _Écrire les communs_ se veut un appel à poursuivre les conversations qui ont accompagné l'élaboration collective du dossier.
- [Sommaire et introduction](http://sens-public.org/article1383.html)

<i class="fa fa-arrow-right"></i> **Organisateur de l'événement _Publishing Sphere_, Montréal 2019**

- trois jours de réflexion et d'ateliers pour expérimenter des formes d’écritures, de publication et de performances susceptibles d’ouvrir des nouveaux espaces d’expression, d’échange et de création, et renouveler ainsi les modalités de construction de l’espace public.
- conception et organisation de l'événement
- financement CRSH Connexion
- développement de la première version de [_pink my pad_](http://notes.ecrituresnumeriques.ca)
- [site officiel](http://publishingsphere.ecrituresnumeriques.ca)
- [archive de l'espace de co-écriture](http://notes.ecrituresnumeriques.ca/2uMYqwveTg-Uy90ELVarfg.html)




<i class="fa fa-arrow-right"></i> **Coordinateur de la refonte de la revue scientifique en ligne Sens Public**

- projet : repenser la revue scientifique à l’ère du numérique
- design et prototypage de la [chaîne éditoriale](https://github.com/EcrituresNumeriques/chaineEditorialSP/) de la revue
- ateliers de co-conception, « grands entretiens » (voir carnet).
- financement FRQSC de la CRC sur les écritures numériques
- [carnet de recherche](http://nicolassauret.net/carnets/) et [prototype](https://framagit.org/ecrinum/sp_hub/)

<i class="fa fa-arrow-right"></i> **Organisateur du séminaire Écritures numériques et éditorialisation**, 2011-2018  

- cycle mensuel en duplex avec l'Université de Montréal  
- partenaires : IRI, Sens Public, Univ. de Montréal, Univ. Paris Ouest Nanterre  
- programme, coordination et animation des sessions  
- [site](seminaire.sens-public.org) et [archives vidéo](http://polemictweet.com/edito/select.php)


<i class="fa fa-arrow-right"></i> **Coordinateur de l'ouvrage collectif _version 0. Notes sur le livre numérique_**

- Conception et coordination d'un _booksprint_ pour la réalisation d'un ouvrage en deux jours (15 participants, 6 éditeurs), dans le cadre du colloque [ÉCRIDIL 2018](http://ecridil.ex-situ.info) – «Écrire, éditer, lire à l'ère numérique»
- [le booksprint](http://ecridil.ex-situ.info/booksprint)
- [l'ouvrage sur OpenLibrary.org](https://openlibrary.org/books/OL26501805M/Version_0._Notes_sur_le_livre_num%C3%A9rique)
- [sources](https://framagit.org/ecrinum/ecridil-booksprint)

<i class="fa fa-arrow-right"></i> **Coordinateur scientifique du [CRIHN](http://www.crihn.org/), le Centre de recherche interuniversitaire sur les humanités numériques**, 2016-2017

<i class="fa fa-arrow-right"></i> **Étude sur _Général Instin_ : dispositif et éditorialisation d'une littérature brouhaha**, 2016-  

- en collaboration avec Servanne Monjour
- [carnet de recherche]({{ site.github.url }}/behindinstin/)

<i class="fa fa-arrow-right"></i> **Étude du corpus Anarchy : expérience transmédia d'écriture collaborative**, 2015-2016  

- en collaboration avec Ariane Mayer
- [carnet de recherche]({{ site.github.url }}/behindanarchy/)


<i class="fa fa-arrow-right"></i> **Co-organisateur de [la 3<sup>ème</sup> journée doctorale](http://www.passes-present.eu/fr/journee-3-comment-chercher-aujourdhui-le-passe-travailler-les-archives-dans-un-contexte-numerique) du Labex _Les passés dans le présent_**, 2016

<i class="fa fa-arrow-right"></i> **Organisation de l’atelier _Pratiques et enjeux de l'éditorialisation_ au Labex Les passés dans le présent**, 2016

- [page de l'atelier](http://www.passes-present.eu/fr/atelier-6-pratiques-et-enjeux-de-leditorialisation-pour-les-passes-dans-le-present-30074)

<i class="fa fa-arrow-right"></i> **Responsable scientifique et coordination du projet ANR Spectacle en ligne(s)**  2013-2015  
-	« Constitution, indexation et usages d'un Corpus de répétitions de spectacle vivant »  
-	partenaires : LIRIS CNRS, INRIA, IRI, Festival d’Aix en Provence, Théâtre des Célestins, Ubicast  
-	[site du projet](http://spectacleenlignes.fr/wp/)

<i class="fa fa-arrow-right"></i> **Co-responsable du projet PROFIL**, 2014  
-	collaboration DICEN, IRI et BDIC, dans le cadre du Labex Les Passés dans le Présent  
-	[page du projet](http://profil.passes-present.eu/)

<i class="fa fa-arrow-right"></i> **Recherche-action avec l’organisation de l’éditorialisation collaborative d’une conférence scientifique**, 2012  
-	conception d’un dispositif collaboratif d’éditorialisation multi-plateforme  
-	30 étudiants mobilisés dans une salle de rédaction  
-	conférence : Les entretiens du nouveau monde industriel 2012 (ENMI2012)  
-	publication : Merzeau, L. (2013). Éditorialisation collaborative d’un événement. Communication et organisation, (43), 105–122. [http://doi.org/10.4000/communicationorganisation.4158](http://doi.org/10.4000/communicationorganisation.4158)

## Publications
### 2022

- Nicolas Sauret. **«&nbsp;Intelligence artificielle & Sciences humaines et sociales (SHS)&nbsp;: opportunités, défis et perspectives &nbsp;»**, I2D - Information, données & documents, vol. 1, no. 1, 2022, pp. 97-103. [DOI : 10.3917/i2d.221.0097](https://www.cairn.info/revue-i2d-information-donnees-et-documents-2022-1-page-97.htm )

### 2021

- Servanne Monjour, Nicolas Sauret. **_Pour une gittérature : l’autorité à l’épreuve du hack_**. Revue XXI-XX, Classiques Garnier, N°2, 2021. [gitlab.com/guido-o/paperjam-2021/](https://gitlab.com/guido-o/paperjam-2021/)
- Nicolas Sauret et Joëlle Le Marec. **«&nbsp;Projeter l’archive. Création et médiations d’un corpus exhaustif de répétitions de spectacles vivants&nbsp;»**, Dans _Fabriques, expériences et archives du spectacle vivant_, sous la direction de Sophie Lucet, Bénédicte Boisson et Marion Denizot. Presses Universitaires de Rennes, 2021

### 2020

- **De la revue au collectif : la conversation comme dispositif d’éditorialisation des communautés savantes en lettres et sciences humaines**. Thèse. Université de Montréal, Université Paris Nanterre. 2020. [these.nicolassauret.net](https://these.nicolassauret.net/)

### 2019

- Sylvia Fredriksson, Nicolas Sauret. 2019. **Préface. Ramener le droit sur Terre**. In Leroy-Terquem, M., & Clément, S. (Eds.), «&nbsp;S.I.Lex, le blog revisité : Parcours de lectures dans le carnet d’un juriste et bibliothécaire&nbsp;». Villeurbanne : Presses de l’enssib. ISBN:[9782375461150](https://www.worldcat.org/isbn/9782375461150). DOI:[10.4000/books.pressesenssib.9497](https://doi.org/10.4000/books.pressesenssib.9497)
- Collectif. **Version 0. Notes Sur Le Livre Numérique.** Montréal: CRC sur les écritures numériques / Codicille éditeur, 2018. 978-2-924446-11-9. [hal-01922682](https://hal.archives-ouvertes.fr/hal-01922682)

### 2018

* Nicolas Sauret. **_Design de la conversation : naissance d’un format éditorial_**. Sciences du design n°2/2018, (Vol.8), 2018. [Sur Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-57.htm)


### 2017

* Nicolas Sauret. **_Epistémologie du modèle&nbsp;: des Humanités syntaxiques&nbsp;?_** Dossier _«&nbsp;Ontologie du numérique&nbsp;»_, sous la direction de S. Monjour, M. Treleani, M. Vitali-Rosati, Revue Sens Public, 2017. [Sur Sens-public.org](http://sens-public.org/article1287.html)

### 2016

* Nicolas Sauret, Ariane Mayer. **_L’autorité dans Anarchy. Les constructions de l’autorité et de l’auctorialité dans un dispositif de production littéraire collaborative : le cas de l’expérience transmédia Anarchy.fr_**. Revue Quaderni n°93, Printemps 2017. [Sur OpenEdition Journals](http://journals.openedition.org/quaderni/1078)
* Joëlle Le Marec, Nicolas Sauret. **_Archivage de répétitions et médiations du spectacle vivant. Le cas du projet Spectacle en ligne(s)_**. Les Cahiers du numérique, n°3/2016 (Vol.12), La médiation des mémoires en ligne, pp.139-164. [Sur Cairn.info](http://www.cairn.info/revue-les-cahiers-du-numerique-2016-3-page-139.htm)

### 2015

* Rémi Ronfard, Benoit Encelle, Nicolas Sauret, Pierre-Antoine Champin, Thomas Steiner, et al.. **_Capturing and Indexing Rehearsals: The Design and Usage of a Digital Archive of Performing Arts_**.  
[Best paper award](http://www.digitalheritage2015.org/awards/) at Digital Heritage, Grenade, Spain.
[Hal-01178053](https://hal.archives-ouvertes.fr/hal-01178053)
* Nicolas Sauret. **_Collaboration Recherche-Musée, une opportunité pour penser l’appropriation des ressources_**. Musées et Recherche – Expérimenter et coopérer : dialogues sur le sens de l’innovation, sous la direction de Joëlle Le Marec et Ewa Maczek. Les dossiers de l'OCIM, 2015, 978-2-11-139614-2

### 2014

* Samuel Huron, Nicolas Sauret et Raphael Velt. **_DesignMetaData, Retour d’expérience sur un atelier de design interactif interdisciplinaire dans une démarche d’innovation ouverte_**. (Interfaces numérique Volume 3 - n°2/2014).  
[Hal-00987995](https://hal.inria.fr/hal-00987995)

## Conférences scientifiques

### 2021
- Nicolas Sauret, Stéphane Pouyllau, Mélanie Bunel. **_Vers un écosystème d'écriture et d'édition avec les données_**. Colloque DHNord 2021 «&nbsp;Publier, partager, réutiliser les données de la recherche&nbsp;: les data papers et leurs enjeux&nbsp;» (actes à paraître)


### 2020

- Nicolas Sauret. **Les institutions fantômes de la littérature numérique** . Colloque «&nbsp;Cartographie du web littéraire francophone&nbsp;», Université Lyon 3, 22-24 janvier 2020 (actes à paraître)

### 2019

- **Publishing practices in scholarly journals : Preliminary results of Revue 2.0**  avec Juliette de Maeyer, Jeanne Hourez, Margot Mellet, Servanne Monjour, Marcello Vitali-Rosati.  
CCA Annual Conference 2019, Congress of the Humanities and Social Sciences, Vancouver, 1-6 juin 2019



### 2018

- **Stylo, éditeur sémantique pour les humanités**  
colloque CRIHN _« Repenser les humanités numériques / Thinking the Digital Humanities Anew »_, Montréal, 25 octobre 2018.
- **L’écriture dispositive du réseau d’écrivains : la conversation comme forme éditoriale**  
panel «Quelle littérature numérique pour demain ?» de la conférence internationale de l’_Electronic Literature Organization « ELO 2018 — Mind the gap! »_, Montréal, 13-18 août 2018
- **Conversations in scholarly journals: experimenting scientific communication with a dynamic and continuous editorial format**  
congrès annuel de la Société Canadienne des Humanités Numériques, Université de Regina, Regina, 26 mai 2018.
- **L’accès libre : un devoir moral et scientifique** avec Joana Casenave, Jean-Claude Guédon, Michael E. Sinatra et Marcello Vitali-Rosati  
conférence virtuelle internationale _Sustainable Research_ organisée par l’Université d’Alberta, Montréal, 2 mai 2018.

### 2017

* **Stylo: Repenser la chaîne éditoriale numérique pour les revues savantes en sciences humaines** [(poster)](https://dh2017.adho.org/abstracts/224/224.pdf)  
conférence internationale _Digital Humanities 2017_ [(ADHO)](http://adho.org/), Montréal
* **Editorialisation et littérature, le cas du Général Instin** [(présentation)](https://ecrituresnumeriques.github.io/sInstinCerisy/)  
colloque _Des humanités numériques littéraires&nbsp;?_, 2017, Cerisy
* **Editorialisation et littérature, le cas du Général Instin**  [(présentation)](https://ecrituresnumeriques.github.io/sInstinToronto/)  
colloque _CSDH-SCHN 2017_ (Société canadienne des humanités numériques) dans le cadre de Congress 2017, Toronto
* **Fabrique de la revue Sens Public : la revue scientifique comme espace public** [(présentation)]({{ site.github. url }}/s_SPacfas/)  
_La publication savante en contexte numérique_, 2017, ACFAS (U. Mc Gill)
* **L'autorité dans Anarchy. Les constructions de l'autorité dans un dispositif de production littéraire collaborative** [(présentation)]({{ site.github. url }}/sAnarchyAutoriteUdem/)  
_Figures et expériences de l'autorité en littérature_, 2017, colloque étudiant du Département des littératures de langue française (UdeM)

### 2016

* **Apologie de la dispersion** [(présentation)]({{ site.github. url }}/behindinstin/2016/12/14/colloque-etudiant-zones-de-dispersion-errance-et-identite.html)  
_Zones de dispersion : errance et identité_, 2016, colloque étudiant du Département de littératures et de langues du monde (UdeM)  
* **What is editorialisation ?**  
CSDH-SCHN 2016 (Société canadienne des humanités numériques), colloque international dans le cadre de Congress 2016
* **Auctorialité, autorité et Anarchy** [(présentation)]({{ site.github. url }}/behindanarchy/2016/05/24/intervention-au-colloque-editorialisation-de-l-auteur.html)  
Colloque international sous la direction de Bertrand Gervais, Servanne Monjour, Marcello Vitali-Rosati (Uqam, Udem)&nbsp;: _Ecrivains, personnage et profils&nbsp;: l’éditorialisation de l’auteur_  
* **L’autorité dans Anarchy. Les constructions de l’autorité dans un dispositif de production littéraire collaborative** [(présentation)]({{ site.github. url }}/behindanarchy/2016/03/16/intervention-au-colloque-mediations-informatisees-de-l-autorite.html)  
colloque organisé par ISCC/CNRS, Celsa/Paris-Sorbonne, UPMC : _Médiations informatisées de l’autorité : nouvelles écritures, nouvelles pratiques de la reconnaissance ?_  

### 2015

* **Vers un framework pour la circulation des connaissances**  
Colloque sous la direction de Marcello Vitali-Rosati, Michael E. Sinatra et Benoît Melançon&nbsp;: *Éditorialisation et nouvelles formes de publication*   

### 2014

* **Présentation des résultats du projet ANR Spectacle en ligne(s)**  
Colloque international à l’Université de Rennes 2 : _Processus de création et archives du spectacle vivant_  
* **Collaboration recherche-musée, une opportunité pour penser l’appropriation des ressources**  
Colloque organisé par OCIM (Université de Bourgogne) et le CERILAC (Université Paris-Diderot) : _Musées et recherche, expérimenter et coopérer : dialogues sur le sens de l'innovation_  
* **Bubble-TV, live visualisation of TV-viewer’s tweets on screen and on stage**  
C-TV Conference 2014, St. Pölten University of Applied Sciences, Austria : _Moving image and multi-device strategies_  

## Conférences et ateliers

### 2021

- **Les Notebooks, enjeux et problématiques en lien avec le cycle des données de la recherche**  
avec Konrad Hinsen, Raphaëlle Krummeich, Laurent Mouchard, Hugues Pecout, Pierre POULAIN et Sébastien Rey-Coyrehourcq   
Journée d'étude normande sur les données de la recherche, IMEC
- **Des programmes au cœur de l’édition scientifique : écritures, appropriations, réutilisations des articles exécutables**  
avec Stéphane Pouyllau et Mélanie Bunel  
Ateliers du colloque DHNord 2021 «&nbsp;Publier, partager, réutiliser les données de la recherche&nbsp;: les data papers et leurs enjeux&nbsp;»



### 2020

- **Les ateliers de [sens public]. Repenser les formats et les formes de l’édition savante**  
avec Servanne Monjour  
Entretiens Jacques Cartier «&nbsp;Libre accès, économie sociale et solidaire, éditorialisation&nbsp;: les nouveaux visages de l’édition savante&nbsp;», 4 novembre 2020

### 2019

- **Stylo, éditeur sémantique pour les humanités**  Colloque international NumeRev, MSH-SUD, Montpellier, 19-21 juin 2019


### 2018

- **Stylo : un éditeur de texte pour les sciences humaines et sociales**, avec Marcello Vitali-Rosati  
 congrès « Édition scientifique 4.0. 8es journées du réseau Médici », Avignon, 18 septembre 2018.
- **table ronde «Publier la recherche»**  
colloque international _Écridil : écrire, éditer, lire à l’ère numérique_, Usine C, Montréal, 30 avril 2018.
- **Conversations in scholarly journals: experimenting editorial format for scientific communication**  
_La Vitrine sur les Humanités Numériques_, Université de Montréal, Montréal, 26 janvier 2018.


### 2016

* **Constitution et médiation d’une archive de répétitions de spectacle vivant**  
Journée d’étude : _Valoriser la recherche en Arts performatifs_
* **De la collection à la base de données : La médiation de l’archive comme terrain transdisciplinaire**  
Séminaire interne Dicen-IDEF : _Mémoires collectives : archives, traces, empreintes_

### 2015

* **Archives, corpus, médiations? La recherche et l’expérimentation culturelle face à leurs productions**  
Ateliers - Dépôt Légal du Web (INA) : _Web et spectacle vivant_
* **Typologie des dispositifs profilaires d’éditorialisation**  
Restitution du projet PROFIL (Labex Les Passés dans le Présent)
* **Analyse du dispositif des ENMI12**  
Webinaire Biens communs numérique du master recherche Infocom (Paris-Ouest)
* **Présentation des résultats et retombées du projet**  
Restitution publique du projet ANR Spectacle en ligne(s)

### 2014

* **Présentation du projet ANR Périplus**  
Conférence MediaExplore de l’AFP à Numa, Paris
* **Présentation du projet ANR Périplus**  
Atelier Paris Machine Learning Group à Numa, Paris

### 2013

* **Le transmédia documentaire, l’archive narrative et participative**  
Ecrans Mobiles 2013, Mobilité, écriture, transmédia, Valence
* **La plateforme Ligne de temps, de l’indexation à l’éditorialisation**  
Atelier Convergence de l’audiovisuel off et online, SATIS, Paris
* **Du montage au mashup**  
Conférence Cinécast à la BPI : _Visionner, annoter, monter ; les nouvelles pratiques du mashup_
